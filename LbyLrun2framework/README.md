# How to run the lbylrun2  code
-**LbyL_truth** contains the code used to produce Minintuples at the true level(no cuts)
# First time setup (on lxplus)
 git clone ssh://git@gitlab.cern.ch:7999/maittaml/lbylanalysisframeworktuto.git  \
 cd  /lbylanalysisframeworktuto/ lbylrun2 \
 source runlbyl.sh
## For the next time  you log in 
source  everylogin.sh
## Add your input files
Path to data here is : /eos/user/m/maittaml/ForTuto/mc
## Run a test
cd .. \
cd run \
python localtest.py \
-You can use **run/runlbyl.py**  to run  the code with diffrenent  options:   \
       -For example: **submit a job to  the grid* , you can use the folloing command:  **python  runlbyl.py  - g  "Name of Rucio dataset" **  \
       - if you have a .txt file with the name containing input file paths, you can use the following command:  **python  runlbyl.py  - t  mc.txt   **
## output files
The job output is stored in the directory 'mclbyl_timestr'. The directory name can be changed in the 
job steering macro "localtest.py" \
Output histograms are stored in files called hist-*.root inside the directory 'mclbyl_timestr'
