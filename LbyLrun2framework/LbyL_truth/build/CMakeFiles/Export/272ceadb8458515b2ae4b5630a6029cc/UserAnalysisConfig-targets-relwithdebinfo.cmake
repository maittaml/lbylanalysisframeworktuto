#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "UserAnalysis::EXCLRunIILib" for configuration "RelWithDebInfo"
set_property(TARGET UserAnalysis::EXCLRunIILib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(UserAnalysis::EXCLRunIILib PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libEXCLRunIILib.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libEXCLRunIILib.so"
  )

list(APPEND _cmake_import_check_targets UserAnalysis::EXCLRunIILib )
list(APPEND _cmake_import_check_files_for_UserAnalysis::EXCLRunIILib "${_IMPORT_PREFIX}/lib/libEXCLRunIILib.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
