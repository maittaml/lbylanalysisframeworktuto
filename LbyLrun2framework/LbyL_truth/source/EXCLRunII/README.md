# Compilation

```
setupATLAS
lsetup git

mkdir myAnalysis
cd myAnalysis
mkdir source 
mkdir build 
mkdir run
mkdir run/histos

cd source 
cp /afs/in2p3.fr/home/m/mdyndal/public/CMakeLists.txt .
git clone https://gitlab.cern.ch/atlas-physics/hi/upc/LbyLRun2Analysis/EXCLRunII.git

cd ../build/
asetup 21.2.38,AnalysisBase
cmake ../source/
make
source */setup.sh

cd ../source/EXCLRunII/scripts
```

*****

**Edit paths/variables in analy_test.par file**
```
#... Base directory for input xAODs (MANDATORY)

ModsHat_dir: "/sps/atlas/m/mdyndal/data_UPC_HI_rel21/"

#...directory with the personal DST data base

DST_DB_dir: "/sps/atlas/m/mdyndal/dst_db.HI_UPC_rel21/"

#... executable (MANDATORY)

executable: /sps/atlas/m/mdyndal/myAnalysis/source/EXCLRunII/scripts/testrun_jobs.py

#... Base directory for the histo tree (optional, default="ModsHat_Dir")

histo_dir: "/sps/atlas/m/mdyndal/myAnalysis/run/histos/"
```
Both ModsHat_dir and DST_DB_dir should have a structure: "year/data(MC)/files", e.g. "15/gg2gg/..."


*****

# Running (batch)
```
cd ../source/EXCLRunII/scripts
./Send_Prod analy_test.par
```
# Running (standalone)
modify dir/file path(s) in testrun.py
```
cd ../source/EXCLRunII/scripts
rm -r submitDir/
python testrun.py
```

# What to do every time you log in
```
setupATLAS
cd ../build
asetup --restore
source */setup.sh
```