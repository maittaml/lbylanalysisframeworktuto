#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
#inputFilePath = '/sps/atlas/m/mdyndal/data_UPC_HI_rel21/15/starlight_ee/group.phys-hi.mc15_5TeV.420052.Starlight_r193_gammagamma2ee_breakupMode2.simul.HITS.e4995_s2820.r21.0.41trkpix_100k_SARA_v1_EXT0/'
#ROOT.SH.ScanDir().filePattern( 'group.phys-hi.13005462.EXT0._000200.AOD.pool.root' ).scan( sh, inputFilePath )
inputFilePath = '/sps/atlas/m/mdyndal/data_UPC_HI_rel21/15/data/user.mdyndal.data15_hi.00287866.physics_UPC.merge.AOD.f984_m2025.HION4.v03_EXT0/'
ROOT.SH.ScanDir().filePattern( 'user.mdyndal.15637842.EXT0._000001.DAOD_HION4.test.pool.root' ).scan( sh, inputFilePath )
sh.Print()

# Create an EventLoop job.
job = ROOT.EL.Job()
#job.useXAOD()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, -1 )


# Create the algorithm's configuration. Note that we'll be able to add
# algorithm property settings here later on.
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
config = AnaAlgorithmConfig( 'LapxAODEvent/AnalysisAlg' )
config2 = AnaAlgorithmConfig( 'EXCLRecoAnalysis/AnalysisAlg2' )

#job.algsAdd( config )
job.algsAdd( config2 )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
