#ifndef HISTMANAGERTRACKING_H
#define HISTMANAGERTRACKING_H

#include <TH1.h>
#include <map>
#include "EXCLRunII/EXCLcandidate.h"
#include "EXCLRunII/ExclVeto.h"
#include "EXCLRunII/Pair.h"
#include "EXCLRunII/HistManager.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

 
class HistManagerTracking { 

  public: 
  
  TString HM_name = "/Tracking/";
  
  std::map<TString, TH1*> bookHistos(TString CurrentShift, TString Analysis);                                 //! books all histograms
  
  void fillHistos(const EXCLCandidate* E, const xAOD::EventInfo* eventInfo, float weight, std::map<TString, TH1*> hm, TString CurrentShift, TString Analysis); //! fills all histograms  
  
};

#endif
