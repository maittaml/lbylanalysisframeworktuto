#ifndef HISTMANAGEREVENT_H
#define HISTMANAGEREVENT_H
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include <TH1.h>
#include <map>

 
class HistManagerEvent { 

  public: 
  
  HistManagerEvent(){ }
  
  TString HM_name = "/Event/";
  
  std::map<TString, TH1*> bookHistos(TString CurrentShift, TString Analysis);                                 //! books all histograms
  
  void fillHistos(const xAOD::EventInfo* eventInfo, float weight, std::map<TString, TH1*> hm, TString CurrentShift, TString Analysis); //! fills all histograms  
  
};

#endif
