/**
 *  @file  EXCLUserParticle.h
 *  @brief  
 *
 *
 *  @author   
 *
 * =====================================================================================
 */


#ifndef __EXCLUSERPARTICLE
#define __EXCLUSERPARTICLE


//--- Root
#include <TObject.h>
#include <TMath.h>
#include <TLorentzVector.h>

//namespace EXCLRun2Analysis
//{


	/**
	 * @class EXCLUserParticle 
	 * @brief Base class for the definition of a  particle
	 */
	class EXCLUserParticle : public TLorentzVector
	{
		protected:

			short m_charge; /**< @brief Electric charge of the particle */
			int m_ID; /**< @brief Id of the particle */



		public :

			/*****************************************************************/

			// Standard constructor and destructor
			EXCLUserParticle();
			EXCLUserParticle(const short charge);
			EXCLUserParticle(const EXCLUserParticle& body);
			EXCLUserParticle(const int ID,const short charge,const float E,const float Pt,const float Eta,const float Phi);
			~EXCLUserParticle();
 
			//ClassDef(EXCLUserParticle, 1);
 
			// Get type of object
			inline int Type(void) const{return m_ID;};
			inline short Charge() const{return m_charge;}

            		inline void SetType(int id) {m_ID=id;}
                        inline void SetCharge(short charge) {m_charge=charge;}

			/**
			 * @brief Relation between particles based on transverse momentum
			 */
			inline bool operator<(const EXCLUserParticle& other){return (Pt()<other.Pt());}

			static bool ordering(EXCLUserParticle* a, EXCLUserParticle* b)
			{
				return (a->Pt() > b->Pt());
			}
			


	};

	inline bool ptOrder(const EXCLUserParticle& p1, const EXCLUserParticle& p2){return (p1.Pt()>p2.Pt());}

// }

#endif

