/**
 *  @file  EXCLRecoAnalysis.h
 *  @brief  full selection of  excl events
 *
 *
 *  @author  
 *
 *  @date    2015-11-07
 *
 *  @internal
 *     Created : 
 * Last update : 
 *          by : 
 *
 * =====================================================================================
 */
 
 
#ifndef EXCLRUNII_EXCLRECOANALYSIS_H
#define EXCLRUNII_EXCLRECOANALYSIS_H

//#include <EventLoop/Algorithm.h>
#include <AnaAlgorithm/AnaAlgorithm.h>

#include <xAODMuon/MuonContainer.h>
#include "xAODRootAccess/tools/Message.h"
#include <EXCLRunII/EXCLUserParticle.h>
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include <xAODTracking/TrackParticle.h>

#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicVariation.h" 

// include files for using the trigger tools
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"


#include <TH1.h>
#include <TH2.h>
#include <TTree.h>

#include "xAODForward/ZdcModuleContainer.h"
//#include "xAODForward/ZdcSumContainer.h"


#include "xAODCore/ShallowCopy.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/PhotonContainer.h"

#include "xAODBase/IParticleHelpers.h"
#include "EXCLRunII/ExclVeto.h"
#include "EXCLRunII/Pair.h"
#include "EXCLRunII/EXCLcandidate.h"

#include "xAODTracking/VertexContainer.h"

#include "xAODTrigMinBias/TrigSpacePointCountsContainer.h"
#include "xAODTrigMinBias/TrigSpacePointCounts.h"
#include "xAODTrigMinBias/versions/TrigHisto2D_v1.h"

#include "xAODHIEvent/HIEventShapeContainer.h"
#include "xAODHIEvent/HIEventShape.h"

#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
//#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"

#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

//#include "PileupReweighting/PileupReweightingTool.h"
#include "AsgTools/ToolHandle.h"

#include "EXCLRunII/AnalysisManager_mmOS.h"
#include "EXCLRunII/AnalysisManager_mmSS.h"
#include "EXCLRunII/AnalysisManager_eeOS.h"
#include "EXCLRunII/AnalysisManager_eeSS.h"
#include "EXCLRunII/AnalysisManager_emuOS.h"
#include "EXCLRunII/AnalysisManager_emuSS.h"

#include "EXCLRunII/AnalysisManager_gg.h"

#include "EXCLRunII/LapxAODEvent.h"





//---- re-define types
typedef std::vector<xAOD::Electron*> VectorOfElectrons;
typedef std::vector<xAOD::Muon*>     VectorOfMuons;


class EXCLRecoAnalysis : public LapxAODEvent
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:




  //---- pointer to the event algo.
  LapxAODEvent * m_xAODEvent; //!
  EL::AnaAlgorithm* ConnectToAnotherAlgorithm(std::string name);
  
  
  
  //---- trigger tools

  asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool; //!
  asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool; //! 
  
  
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!
  
  
  
  bool eventFiredHLT_hi_gg_upc_L1TE5_VTE200(void);
  bool eventFiredHLT_mb_sptrk_ion_L1ZDC_A_C_VTE50(void);  
  bool eventFiredHLT_mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50(void);  
  bool eventFiredHLT_hi_loose_upc_L1ZDC_A_C_VTE50(void);
  bool eventFiredHLT_mb_sptrk_vetombts2in_L1MU0_VTE50(void);  
  bool eventFiredHLT_TE5(void);
  
  bool eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_noiseSup_L1TEX_VTEY(void);
  bool eventFired_2018_HLT_hi_upc_FgapAC3_hi_gg_upc_L1TAUX_TEY_VTEZ(void);
  bool eventFired_2018_exclusivelooseFgapTriggers(void);
  bool eventFired_2018_exclusivelooseNoFgapTriggers(void);
  bool eventFired_2018_EMPTY(void);



  int num2muon; //!
  int num2ele;   //!
  int num34muon;   //!
  int numforTotalevents; //!
  int numfor2ElectronsEvent; //!
  int numfor2PhotonsEvent; //!
  int CutFlowMu_TotalNumber; //!
  int CutFlowMu_15GeV; //!
  int CutFlowMu_15GeV_d0sig;   //!
  int CutFlowMu_15GeV_d0sig_z0;  //! 
   
  int CutFlowEl_TotalNumber; //!
  int CutFlowEl_15GeV; //!
  int CutFlowEl_15GeV_d0sig;  //! 
  int CutFlowEl_15GeV_d0sig_z0; //!
  
  int zeropileup;   //!
  
  TH1F *h_Clusters_SumEt_2015; //!
  
  TH1F *h_Clusters_SumEt_2018_noiseSup; //!
  TH1F *h_Clusters_SumEt_2018_L1TAU; //!
  TH1F *h_Clusters_SumEt_2018_excllooseNoFgap; //!
  TH1F *h_Clusters_SumEt_2018_excllooseFgap; //!
  
  TH1F *h_Clusters_SumEt_2018_excllooseFgap_ZDC_AND; //!
  TH1F *h_Clusters_SumEt_2018_excllooseFgap_ZDC_XOR; //!
  TH1F *h_Clusters_SumEt_2018_excllooseFgap_VZDC; //!
  
  TH1F *h_Clusters_SumEt_2018_noiseSup_AND_excllooseNoFgap; //!
  TH1F *h_Clusters_SumEt_2018_noiseSup_AND_excllooseFgap; //!
  TH1F *h_Clusters_SumEt_2018_L1TAU_AND_excllooseNoFgap; //!
  TH1F *h_Clusters_SumEt_2018_L1TAU_AND_excllooseFgap; //!
  TH1F *h_Clusters_SumEt_2018_L1TAU_OR_TE4_AND_excllooseFgap; //!

  TH1F *h_PixelSum_2018_noiseSup; //!
  TH1F *h_PixelSum_2018_L1TAU; //!
  TH1F *h_PixelSum_2018_excllooseFgap; //!
  TH1F *h_PixelSum_2018_excllooseNoFgap; //!

  TH1F *h_FgapA_2018_noiseSup; //!
  TH1F *h_FgapA_2018_L1TAU; //!
  TH1F *h_FgapC_2018_noiseSup; //!
  TH1F *h_FgapC_2018_L1TAU; //!  
  TH1F *h_FgapA_2018_excllooseNoFgap; //!
  TH1F *h_FgapC_2018_excllooseNoFgap; //!
  
  
  TH1F *LbLSel_CtrlNoAcoNoPixTrk_nMuons; //! 
  TH1F *LbLSel_CtrlNoAcoNoPixTrk_nMuons2; //!
  TH1F *electron_studies_2018_L1TAU_nMuons; //! 
  TH1F *electron_studies_2018_L1TAU_nMuons2; //!
  TH1F *LbLSel_CtrlNoAcoPt_nMuons; //! 
  TH1F *LbLSel_1MCut_EMPTY_nMuons; //!

/////added by hassane///

 TH1F *h_nMuons_nominal; //! 
 TH1F *h_nMuons; //! 
////////////////
  
  TH1F *LbLSel_5MuonVeto_ZDCsumsA; //!
  TH1F *LbLSel_5MuonVeto_ZDCsumsC; //!
  TH1F *LbLSel_5MuonVeto_ZDCsumsAC; //!
TH1F *h_nphotons ; //! 
  TH1F *electron_studies_2018_L1TAU_ZDCsumsA; //!
  TH1F *electron_studies_2018_L1TAU_ZDCsumsC; //!
  
  TH1F *h_fakeABCD_B; //!
  TH1F *h_fakeABCD_C; //!
  TH1F *h_fakeABCD_D; //!

  TH1F* h_eeg_F1; 
  TH1F* h_eeg_Reta;
  TH1F* h_eeg_Weta2; 
  TH1F* h_eeg_Eratio; 
  TH1F* h_eeg_Rhad;
  TH1F* h_eeg_Rhad1;
  
 
  TH2F *h_NTrkRecoTruth; //!
  TH1F *h_NTrkTruthAll;//!
  TH1F *h_NTrkTruthVtxMatched;//!
  
  TH1F *h_DEt34;//!
  TH1F *h_DEt45;//!
  TH1F *h_DEt57;//!
  TH1F *h_DEt710;//!
  TH1F *h_DEt1015;//!
  
  TH1F *h_zdca;//!
  TH1F *h_zdcc;//!

  TH1F *h_em_aco;//!
  TH1F *h_em_ptmu;//!
  TH1F *h_em_pte;//!
  TH1F *h_em_ngam;//!
  TH1F *h_em_deta;//!

  TH1F *h_3g_m;//!
  TH1F *h_3g_pt;//!
  TH1F *h_3g_ngam;//!
    
  TH1F *h_npix_hits_1_HLT_hi_gg_upc_L1TE5_VTE200_2Tracks;//!
  TH1F *h_npix_hits_1_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_2Tracks_TE5;//!
  TH1F *h_npix_hits_2_LbLSel_4NTrkCutEt3GeV;//!
  TH1F *h_npix_hits_2_LbLSel_CtrlNoAco;//!
  TH1F *h_npix_hits;//!
  
  
  TH1F *h_ElePtTag;//!
  TH1F *h_ElePtProbe;//!
  TH1F *h_EleEtaTag;//!
  TH1F *h_EleEtaProbe;//!

  TH1F *h_ElePtTagB;//!
  TH1F *h_ElePtProbeB;//!
  TH1F *h_ElePtTagEC;//!
  TH1F *h_ElePtProbeEC;//!
  TH1F *h_ElePtTagBEC;//!
  TH1F *h_ElePtProbeBEC;//!

  
  TH1F *h_EleTrkTag;//!
  TH1F *h_EleTrkProbe;//!
  TH1F *h_EleGamPt;//!
  TH1F *h_EleGamEta;//!
  TH1F *h_EleGamDR;//!
  TH1F *h_EleGamPt3;//!
  TH1F *h_EleGamPt3ID;//!
  
  TH1F *h_EleTrkPt;//!
  TH1F *h_ElePtllg;//!
  
  TH1F *h_EGMisID1;//!
  TH1F *h_EGMisID2;//!
  TH1F *h_EGMisID1Iso;//!
  TH1F *h_EtSinglePhoton;//!
  
  TH1F *h_AcoEG;//!
  TH1F *h_AcoEG2;//!
  TH1F *h_Ntrk1;//!
  TH1F *h_Ntrk2;//! 
  TH1F *h_Author;//!
  TH1F *h_4gMass;//!
  TH1F *h_2trkMass;//!

  TH1F *h_PtEG;//!
  TH1F *h_PtEG2;//!
  
  TH1F *h_trkPtEG;//!

  TH1F *h_TruthMass;//!
  TH1F *h_TruthMass3GeV;//!
  TH1F *h_TruthMass35GeV;//!
  
  TH1F *h_TruthPt;//!
  TH1F *h_TruthPt1;//!
  TH1F *h_TruthPt3;//!
  TH1F *h_TruthEta;//!
  TH1F *h_TruthEta1;//!

  TH1F *h_TE5eff1;//!
  TH1F *h_TE5eff2;//!
  
  TH1F *h_RunDependence;//!
  
  const double Mz = 91.1876;//! 
  const double MzWidth = 2.4952; //!
  const double Mw = 80.385; //!
  const double MwWidth = 2.085; //!

  //--- standard constructor
  EXCLRecoAnalysis (const std::string& name, ISvcLocator* pSvcLocator);


   
  std::vector<CP::SystematicSet> m_sysList; //!

  
  // defining the output file name and tree that we will put in the output ntuple, also the one branch that will be in that tree 
  std::string outputName; //!
////////////TTree Branches////////////////////////
 std::string m_outputName="ANALYSIS";
  TTree *m_tree; //!
 //  std::map<TString, TTree*> m_tree; //!


  int m_EventNumber; //!
int m_RunNumber ; //!
int m_AverageMu ; //!
/////muons///////////
int m_nMuons_sel ; //!
int m_nMuons_nominal ; //!

/////electrons///////////
int m_nElectrons ; //!
std::vector<Float_t> m_Electron_Pt ; //!
//std::vector<Float_t> m_Electron2_Pt ; //!
std::vector<Float_t> m_Electron_eta ; //!
//std::vector<Float_t> m_Electron2_eta ; //!
std::vector<Float_t> m_Electron_phi ; //!
//std::vector<Float_t> m_Electron2_phi ; //!
std::vector<Float_t> m_Electron_d0 ; //!
//std::vector<Float_t> m_Electron2_d0 ; //!
std::vector<Float_t> m_Electron_z0 ; //!
//std::vector<Float_t> m_Electron2_z0 ; //!
std::vector<Float_t> m_ee_pt ; //!
std::vector<Float_t> m_ee_Mass ; //!
std::vector<Float_t> m_ee_Acoplanarity ; //!
/////photons///////////
int m_nPhotons ; //!
std::vector<Float_t> m_Photon_Pt ; //!
//std::vector<Float_t> m_Photon2_Pt ; //!
std::vector<Float_t> m_Photon_eta ; //!
//std::vector<Float_t> m_Photon2_eta ; //!
std::vector<Float_t> m_Photon_phi ; //!
//std::vector<Float_t> m_Photon2_phi ; //!
std::vector<Float_t> m_Photon_d0 ; //!
//std::vector<Float_t> m_Photon2_d0 ; //!
std::vector<Float_t> m_Photon_z0 ; //!
//std::vector<Float_t> m_Photon2_z0 ; //!
std::vector<Float_t> m_gg_pt ; //!
std::vector<Float_t> m_gg_Mass ; //!
std::vector<Float_t> m_gg_Acoplanarity ; //!
///////////////////////////////////////////////////////
/////Tracks///////////
int m_nTracks ; //!
std::vector<Float_t> m_Track_Pt ; //!
//std::vector<Float_t> m_Track2_Pt ; //!
std::vector<Float_t> m_Track_eta ; //!
//std::vector<Float_t> m_Track2_eta ; //!
std::vector<Float_t> m_Track_phi ; //!
//std::vector<Float_t> m_Track2_phi ; //!
std::vector<Float_t> m_Track_z0 ; //!

  PairCandidate *findPairCandidates(	const std::vector<xAOD::Electron*> goodElectrons,
  				const std::vector<xAOD::Muon*> goodMuons,
				const std::vector<xAOD::Photon*> goodPhotons,
				const std::vector<xAOD::TrackParticle*> goodTracks
				); 
  
  double LeptScaleFactors(PairCandidate *m_Pair);
  double EleEffTrkPtSF(xAOD::Electron *ele11);
  double EleEffTrkPtSF(xAOD::TrackParticle *trk11);
  double EleTagPtWeight(xAOD::Electron *ele11);
  
  
   
    
  // these are the functions inherited from Algorithm
  //virtual StatusCode setupJob (Job& job);
  virtual StatusCode fileExecute ();
  virtual StatusCode histInitialize ();
  virtual StatusCode changeInput (bool firstFile);
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode postExecute ();
  virtual StatusCode finalize () override;
  virtual StatusCode histFinalize ();


  std::map<TString, TH1*> Histogram_map; //!
/////////////////////////////////truth declaration //////////////////////////
   // Following: * m_genWMuon is a pointer 
  EXCLUserParticle* m_genZ; //!
  EXCLUserParticle* m_genW; //!  

  EXCLUserParticle* m_genWMuonDRESS; //!
  EXCLUserParticle* m_genZMuon1DRESS; //!
  EXCLUserParticle* m_genZMuon2DRESS; //!
  EXCLUserParticle* m_genWNeutrinoMu; //! 

EXCLUserParticle* m_genWPhotonDRESS; //!
  EXCLUserParticle* m_genZPhoton1DRESS; //!
  EXCLUserParticle* m_genZPhoton2DRESS; //!


  EXCLUserParticle* m_genWElectronDRESS; //! 
  EXCLUserParticle* m_genZElectron1DRESS; //! 
  EXCLUserParticle* m_genZElectron2DRESS; //!
  EXCLUserParticle* m_genWNeutrinoEle; //! 

  void PrintIDStatusPart (const xAOD::TruthParticle* tx); //!
  void PrintOrigtExtractedPart( int  us_pdgId, uint  us_status,  uint  us_barcode, uint us_type, uint us_orig ); //!
////////////////////////////////////////////////////////////////////////////
  void bookHistos(TString SystematicName);                        //! books all histograms
  void fillNoCutsHistos(const xAOD::EventInfo* eventInfo, TString SystematicName); //! fills all histograms  
  void fillHistos(const EXCLCandidate* E, const xAOD::EventInfo* eventInfo, float weight, TString SystematicName); //! fills all histograms   
  
  bool trackIso(xAOD::Photon* goodPhoton, const std::vector<xAOD::TrackParticle*> goodTracks); 
 
 
   bool   isGoodDataEvent();
  
  AnalysisManager_mmOS m_Analysis_mmOS; //!
  AnalysisManager_mmSS m_Analysis_mmSS; //!
  AnalysisManager_eeOS m_Analysis_eeOS; //!
  AnalysisManager_eeSS m_Analysis_eeSS; //!

  AnalysisManager_emuOS m_Analysis_emuOS; //!
  AnalysisManager_emuSS m_Analysis_emuSS; //!
  
  AnalysisManager_gg m_Analysis_gg; //!
  
  TH1 *h_Zall; //!
   
   
  bool DoSyst = true; //!
   
  // this is needed to distribute the algorithm to the workers
  //ClassDef(EXCLRecoAnalysis, 1);
  
  private: 
  

  
  protected:  
  

  
};


#endif
