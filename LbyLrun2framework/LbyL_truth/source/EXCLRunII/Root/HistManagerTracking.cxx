#include "EXCLRunII/HistManagerTracking.h"

std::map<TString, TH1*> HistManagerTracking :: bookHistos(TString CurrentShift, TString Analysis) {
//Book all histograms here

  std::map<TString, TH1*> hm;
  hm.clear();
  
  TH1* h = NULL; 

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_NVtx",	   "Vertex multiplicity",    25,  -0.5, 24.5); // 
  hm[h->GetName()] = h;  
  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_NTrk",   "Total track multiplicity",    20,  -0.5, 19.5); // 
  hm[h->GetName()] = h;  

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_Trk_eta",  "eta of track",    50,  -2.5, 2.5); // 
  hm[h->GetName()] = h; 
 
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_Trk_pt",  "pt of track",    500,  0., 5.); // 
  hm[h->GetName()] = h; 
  

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_NPixTrk",   "Pixel track multiplicity",    20,  -0.5, 19.5); // 
  hm[h->GetName()] = h;


  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_PixTrk_d0",	"d0 of pixel track",  200.,  -10., 10.); 
  hm[h->GetName()] = h; 

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_PixTrk_eta",  "eta of pixel track",    50,  -2.5, 2.5); // 
  hm[h->GetName()] = h; 
 
   h  = new TH1F(CurrentShift + Analysis + HM_name + "h_PixTrk_pt",  "pt of pixel track",    500,  0., 5.); // 
  hm[h->GetName()] = h;  


  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_zPVtx",   "PV z coordinate",    300,  -150, 150); // 
  hm[h->GetName()] = h;

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_zLepVtx",   "Lepton Vtx z coordinate",    300,  -150, 150); // 
  hm[h->GetName()] = h;  
  

    

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_Lepd0",	"Leptond0",  200.,  -1., 1.); 
  hm[h->GetName()] = h;   
  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_Lepz0_wrtPV",	"Leptonz0_wrtPV",  400.,  -20., 20.); 
  hm[h->GetName()] = h; 

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_Lepz0_wrtLepVtx",	"Leptonz0_wrtLepVtx",  400.,  -20., 20.); 
  hm[h->GetName()] = h;   

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_nTotPixHits",  "Total Pixel hits",    20,  -0.5, 19.5); // 
  hm[h->GetName()] = h; 
  
  return hm;
}

void HistManagerTracking :: fillHistos(const EXCLCandidate* E, const xAOD::EventInfo* eventInfo, float weight, std::map<TString, TH1*> hm, TString CurrentShift, TString Analysis){
  
  //ExclVeto
  
  const ExclVetoCandidate *V  = E->ExclVetocandidate();
  
  hm[CurrentShift + Analysis + HM_name + "h_NVtx"] -> Fill(V->nVtx(),  weight);
  
  hm[CurrentShift + Analysis + HM_name + "h_NTrk"] -> Fill(V->nTrkAll(),  weight);
  
  hm[CurrentShift + Analysis + HM_name + "h_NPixTrk"] -> Fill(V->nTrkPri(),  weight);	


    for(unsigned int trki=0; trki < V->fullTrkList().size(); trki++)
    { 	
	hm[CurrentShift + Analysis + HM_name + "h_Trk_eta"] -> Fill(V->fullTrkList().at(trki)->eta(),  weight);
	hm[CurrentShift + Analysis + HM_name + "h_Trk_pt"] -> Fill(V->fullTrkList().at(trki)->pt()/1e3,  weight);     
    } 


    for(unsigned int trki=0; trki < V->priTrkList().size(); trki++)
    { 	
	hm[CurrentShift + Analysis + HM_name + "h_PixTrk_eta"] -> Fill(V->priTrkList().at(trki)->eta(),  weight);
	hm[CurrentShift + Analysis + HM_name + "h_PixTrk_pt"] -> Fill(V->priTrkList().at(trki)->pt()/1e3,  weight);
	hm[CurrentShift + Analysis + HM_name + "h_PixTrk_d0"] -> Fill(V->priTrkList().at(trki)->d0(),  weight);     
    }  
  
/* 
 
  if(V->priVtx()) hm[CurrentShift + Analysis + HM_name + "h_zPVtx"] -> Fill(V->priVtx()->z(),  weight);
  
  const PairCandidate *Z = E->Paircandidate();


    for(unsigned int trki=0; trki < V->fullTrkList().size(); trki++)
    { 	
	hm[CurrentShift + Analysis + HM_name + "h_TrkEtaRS"] -> Fill(V->fullTrkList().at(trki)->eta(),  weight);
	hm[CurrentShift + Analysis + HM_name + "h_TrkPtRS"] -> Fill(V->fullTrkList().at(trki)->pt()/1.e3,  weight);     
    }  

//////////////////////////////////////////////////////////////////////////////////////////////
    int nPixHits = 0;
    for(unsigned int trki=0; trki < V->priTrkList().size(); trki++)
    { 	
	
       uint8_t pix1 = 0;
       V->priTrkList().at(trki)->summaryValue(pix1,xAOD::numberOfPixelHits);
       nPixHits += pix1;    
    }  
    hm[CurrentShift + Analysis + HM_name + "h_nTotPixHits"] -> Fill(nPixHits,  weight);
/////////////////////////////////////////////////////////////////////////////////////////////


  if (Z->type() == 22) {  


    float z_from_leptons = 0.;
    if(V->priVtx()) z_from_leptons = V->priVtx()->z();
    
    
    int ntrkRS = 0;
    for(unsigned int trki=0; trki < V->fullTrkList().size(); trki++)
    { 
      
      if ( fabs(V->fullTrkList().at(trki)->d0()) > 1.5 ) continue;
      
      float trk_z0 = (V->fullTrkList().at(trki))->z0();
      float beamspot_z0 = (V->fullTrkList().at(trki))->vz();
      float theta = (V->fullTrkList().at(trki))->theta();

      if( fabs((trk_z0 - z_from_leptons + beamspot_z0)*sin(theta)) < 1.5 ){
	ntrkRS++;
	
	hm[CurrentShift + Analysis + HM_name + "h_TrkEtaRS"] -> Fill(V->fullTrkList().at(trki)->eta(),  weight);
	hm[CurrentShift + Analysis + HM_name + "h_TrkPtRS"] -> Fill(V->fullTrkList().at(trki)->pt()/1.e3,  weight);
      }


     
    }   
  
  hm[CurrentShift + Analysis + HM_name + "h_NTrkggv"] -> Fill(ntrkRS,  weight); 
    
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////


  if (Z->type() == 13 || Z->type() == 1313) {  

    const xAOD::TrackParticle *mutrk1 = Z->muon1()->primaryTrackParticle();
    const xAOD::TrackParticle *mutrk2 = Z->muon2()->primaryTrackParticle();
    float z_from_leptons = 0.5*((mutrk1->z0() + mutrk1->vz()) + (mutrk2->z0() + mutrk2->vz()));
    
    hm[CurrentShift + Analysis + HM_name + "h_zLepVtx"] -> Fill( z_from_leptons,  weight);    

    hm[CurrentShift + Analysis + HM_name + "h_Lepd0"] -> Fill( mutrk1->d0(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_Lepd0"] -> Fill( mutrk2->d0(),     weight); 
    
    //hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtPV"] -> Fill( mutrk1->z0() + mutrk1->vz() - V->priVtx()->z(),     weight); 
    //hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtPV"] -> Fill( mutrk2->z0() + mutrk2->vz() - V->priVtx()->z(),     weight); 
    
    hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtLepVtx"]-> Fill( mutrk1->z0() + mutrk1->vz() - z_from_leptons,  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtLepVtx"]-> Fill( mutrk2->z0() + mutrk2->vz() - z_from_leptons,  weight); 


    ////////////////////////// filling random sampling variables ////////

    double rndmvx_z0 = gRandom->Gaus(-5.98588e+00, 4.42579e+01);
    bool passMinDist = false;
  
 //   while( fabs( rndmvx_z0  - z_from_leptons ) < 15. ) 
  //  {
   	rndmvx_z0 = gRandom->Gaus(-5.98588e+00, 4.42579e+01);
  //  }

    
    if( fabs( rndmvx_z0  - z_from_leptons ) > 6. ) passMinDist = true;
        

   
    bool passRS = true;
    //std::vector< xAOD::TrackParticle *> m_rsTrkList;
    double isosize = 500.;
    int ntrkRS = 0;
    int ntrk20mm = 0;
    int ntrkFixed20mm = 0;
    int ntrkMatched = 0;
   
    for(unsigned int trki=0; trki < V->fullTrkList().size(); trki++)
    { 
   
      float trk_z0 = (V->fullTrkList().at(trki))->z0();
      float beamspot_z0 = (V->fullTrkList().at(trki))->vz();
      float theta = (V->fullTrkList().at(trki))->theta();
//      double trk_d0sig = xAOD::TrackingHelpers::d0significance( V->fullTrkList().at(trki)), eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
   
      if ( fabs(V->fullTrkList().at(trki)->d0()) > 1. ) continue;
      
      // --- dR distance lepton -- track (to remove lepton tracks from tracking container)
      double lep1_dist = sqrt( (Z->muon1()->phi() - V->fullTrkList().at(trki)->phi())*(Z->muon1()->phi() - V->fullTrkList().at(trki)->phi())
      		       +  (Z->muon1()->eta() - V->fullTrkList().at(trki)->eta())*(Z->muon1()->eta() - V->fullTrkList().at(trki)->eta()) );
      double lep2_dist = sqrt( (Z->muon2()->phi() - V->fullTrkList().at(trki)->phi())*(Z->muon2()->phi() - V->fullTrkList().at(trki)->phi())
      		       +  (Z->muon2()->eta() - V->fullTrkList().at(trki)->eta())*(Z->muon2()->eta() - V->fullTrkList().at(trki)->eta()) );
      
      if ( lep1_dist < 0.01 || lep2_dist < 0.01 )
      {
         ntrkMatched++;
	 continue;
      }
      	            
      //--- iso size wrt lepton vertex! 
      double z_dist = fabs((trk_z0 - z_from_leptons + beamspot_z0));     
      if( z_dist < isosize ) isosize = z_dist;

      if( z_dist > 20.0 ){
        ntrk20mm++;
	hm[CurrentShift + Analysis + HM_name + "h_Trkd020mm"] -> Fill(V->fullTrkList().at(trki)->d0(),  weight);      
      }
      
      if( fabs((trk_z0 - z_from_leptons - 20. + beamspot_z0)) < 1.0 ) ntrkFixed20mm++;

      
     
      //--- random sampling wrt random point (with safe distance from lepton vertex)
      if( fabs((trk_z0 - rndmvx_z0 + beamspot_z0)) < 1.0 && passMinDist){
        passRS = false;
	ntrkRS++;
        //m_rsTrkList.push_back( V->fullTrkList().at(trki) );
	hm[CurrentShift + Analysis + HM_name + "h_TrkEtaRS"] -> Fill(V->fullTrkList().at(trki)->eta(),  weight);
	hm[CurrentShift + Analysis + HM_name + "h_TrkPtRS"] -> Fill(V->fullTrkList().at(trki)->pt(),  weight);
      }


     
    }   
    
    hm[CurrentShift + Analysis + HM_name + "h_LepVtxIso"] -> Fill(isosize,  weight);
    hm[CurrentShift + Analysis + HM_name + "h_NTrkMatched"] -> Fill(ntrkMatched,  weight);
    hm[CurrentShift + Analysis + HM_name + "h_NTrk20mm"] -> Fill(ntrk20mm,  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_NTrkFixed20mm"] -> Fill(ntrkFixed20mm,  weight);
    
    if(passRS && passMinDist) {  
      hm[CurrentShift + Analysis + HM_name + "h_NVtxRS"] -> Fill(V->nVtx(),  weight);  
    }

    if(passMinDist) {  
      hm[CurrentShift + Analysis + HM_name + "h_NVtxRS2"] -> Fill(V->nVtx(),  weight); 
      hm[CurrentShift + Analysis + HM_name + "h_NTrkRS"] -> Fill(ntrkRS,  weight); 
      hm[CurrentShift + Analysis + HM_name + "h_zRSVtx"] -> Fill(rndmvx_z0,  weight);
    }

    //m_rsTrkList.clear();
    
  }
  


  if (Z->type() == 11 || Z->type() == 1111) {  

    const xAOD::TrackParticle* tp1 = Z->electron1()->trackParticle();
    const xAOD::TrackParticle* tp2 = Z->electron2()->trackParticle();
    float z_from_leptons = 0.5*((tp1->z0() + tp1->vz()) + (tp2->z0() + tp2->vz()));
    
    hm[CurrentShift + Analysis + HM_name + "h_zLepVtx"] -> Fill( z_from_leptons,  weight);    

    hm[CurrentShift + Analysis + HM_name + "h_Lepd0"] -> Fill( tp1->d0(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_Lepd0"] -> Fill( tp2->d0(),     weight); 
    
    //hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtPV"] -> Fill( tp1->z0() + tp1->vz() - V->priVtx()->z(),     weight); 
    //hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtPV"] -> Fill( tp2->z0() + tp2->vz() - V->priVtx()->z(),     weight); 
    
    hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtLepVtx"]-> Fill( tp1->z0() + tp1->vz() - z_from_leptons,  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtLepVtx"]-> Fill( tp2->z0() + tp2->vz() - z_from_leptons,  weight); 

    bool passRS = V->passedRandomSampling(z_from_leptons, 2.);  
    if(passRS) {  
      hm[CurrentShift + Analysis + HM_name + "h_NVtxRS"] -> Fill(V->nVtx(),  weight);  
    }
    
  }  

  if (Z->type() == -1113 || Z->type() == 1311) {  

    const xAOD::TrackParticle* tp1 = Z->electron1()->trackParticle();
    const xAOD::TrackParticle* tp2 = Z->muon1()->primaryTrackParticle();
    float z_from_leptons = 0.5*((tp1->z0() + tp1->vz()) + (tp2->z0() + tp2->vz()));
    
    hm[CurrentShift + Analysis + HM_name + "h_zLepVtx"] -> Fill( z_from_leptons,  weight);    

    hm[CurrentShift + Analysis + HM_name + "h_Lepd0"] -> Fill( tp1->d0(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_Lepd0"] -> Fill( tp2->d0(),     weight); 
    
    //hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtPV"] -> Fill( tp1->z0() + tp1->vz() - V->priVtx()->z(),     weight); 
    //hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtPV"] -> Fill( tp2->z0() + tp2->vz() - V->priVtx()->z(),     weight); 
    
    hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtLepVtx"]-> Fill( tp1->z0() + tp1->vz() - z_from_leptons,  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_Lepz0_wrtLepVtx"]-> Fill( tp2->z0() + tp2->vz() - z_from_leptons,  weight); 

//    bool passRS = V->passedRandomSampling(z_from_leptons, 2.);  
//    if(passRS) {  
//      hm[CurrentShift + Analysis + HM_name + "h_NVtxRS"] -> Fill(V->nVtx(),  weight);  
//    }
    
  }  
  
  */

}
