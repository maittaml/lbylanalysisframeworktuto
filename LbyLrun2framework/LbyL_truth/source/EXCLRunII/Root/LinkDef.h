

#include <EXCLRunII/LapxAODEvent.h>
#include <EXCLRunII/EXCLRecoAnalysis.h>


#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class LapxAODEvent+;
#pragma link C++ class EXCLRecoAnalysis+;

#endif
