#include "EXCLRunII/HistManagerElectron.h"

std::map<TString, TH1*> HistManagerElectron :: bookHistos(TString CurrentShift, TString Analysis) {
  
  //Book all histograms here

  std::map<TString, TH1*> hm;
  hm.clear();

  TH1* hElectronPt   = new TH1F(CurrentShift + Analysis + HM_name + "h_ElectronPt",	"ElectronPt",	  50,   0., 25.); 
  hm[hElectronPt->GetName()] = hElectronPt;  
    
  TH1* hElectronEta  = new TH1F(CurrentShift + Analysis + HM_name + "h_ElectronEta",	"ElectronEta",    60.,  -3., 3.); 
  hm[hElectronEta->GetName()] = hElectronEta;
  
  TH1* hElectronPhi  = new TH1F(CurrentShift + Analysis + HM_name + "h_ElectronPhi",	"ElectronPhi",    64.,  -3.2, 3.2); 
  hm[hElectronPhi->GetName()] = hElectronPhi;  


  TH1* hElectrond0  = new TH1F(CurrentShift + Analysis + HM_name + "h_Electrond0",	"Electrond0",    200.,  -1., 1.); 
  hm[hElectrond0->GetName()] = hElectrond0;

  TH1* hElectronz0  = new TH1F(CurrentShift + Analysis + HM_name + "h_Electronz0",	"Electronz0",    200.,  -2., 2.); 
  hm[hElectronz0->GetName()] = hElectronz0;

  TH1* hElectronNPix  = new TH1F(CurrentShift + Analysis + HM_name + "h_ElectronNPix",	"ElectronNPix",    10.,  -0.5, 9.5); 
  hm[hElectronNPix->GetName()] = hElectronNPix;


  TH1* hPhotonF1   = new TH1F(CurrentShift + Analysis + HM_name + "h_PhotonF1",	"PhotonF1",	  50,   0., 1.); 
  hm[hPhotonF1->GetName()] = hPhotonF1;  
  TH1* hPhotonReta   = new TH1F(CurrentShift + Analysis + HM_name + "h_PhotonReta",	"PhotonReta",	  50,   0.5, 1.5); 
  hm[hPhotonReta->GetName()] = hPhotonReta; 
  TH1* hPhotonWeta2   = new TH1F(CurrentShift + Analysis + HM_name + "h_PhotonWeta2",	"PhotonWeta2",	  60,   0.0, 0.03); 
  hm[hPhotonWeta2->GetName()] = hPhotonWeta2;
  TH1* hPhotonEratio   = new TH1F(CurrentShift + Analysis + HM_name + "h_PhotonEratio",	"PhotonEratio",	  60,   0.0, 1.2); 
  hm[hPhotonEratio->GetName()] = hPhotonEratio;
  TH1* hPhotonRhad  = new TH1F(CurrentShift + Analysis + HM_name + "h_PhotonRhad",	"PhotonRhad",	  40,   -0.2, 0.2); 
  hm[hPhotonRhad->GetName()] = hPhotonRhad;
  TH1* hPhotonRhad1  = new TH1F(CurrentShift + Analysis + HM_name + "h_PhotonRhad1",	"PhotonRhad1",	  40,   -0.2, 0.2); 
  hm[hPhotonRhad1->GetName()] = hPhotonRhad1;

  TH1* hElectronDEt34   = new TH1F(CurrentShift + Analysis + HM_name + "h_ElectronDEt34",	"ElectronDEt34", 100,   -0.5, 0.5); 
  hm[hElectronDEt34->GetName()] = hElectronDEt34;
  TH1* hElectronDEt45   = new TH1F(CurrentShift + Analysis + HM_name + "h_ElectronDEt45",	"ElectronDEt45", 100,   -0.5, 0.5); 
  hm[hElectronDEt45->GetName()] = hElectronDEt45;
  TH1* hElectronDEt57   = new TH1F(CurrentShift + Analysis + HM_name + "h_ElectronDEt57",	"ElectronDEt57", 100,   -0.5, 0.5); 
  hm[hElectronDEt57->GetName()] = hElectronDEt57;
  TH1* hElectronDEt710   = new TH1F(CurrentShift + Analysis + HM_name + "h_ElectronDEt710",	"ElectronDEt710", 100,   -0.5, 0.5); 
  hm[hElectronDEt710->GetName()] = hElectronDEt710;
  TH1* hElectronDEt1015   = new TH1F(CurrentShift + Analysis + HM_name + "h_ElectronDEt1015",	"ElectronDEt1015", 100,   -0.5, 0.5); 
  hm[hElectronDEt1015->GetName()] = hElectronDEt1015;


  TH1* hPhotonConv  = new TH1F(CurrentShift + Analysis + HM_name + "h_PhotonConv",	"PhotonConv",    7.,  -0.5, 6.5); 
  hm[hPhotonConv->GetName()] = hPhotonConv;
  
  return hm;
}

void HistManagerElectron :: fillHistos(const EXCLCandidate* E, float weight, std::map<TString, TH1*> hm, TString CurrentShift, TString Analysis) {
  
  const PairCandidate *Z = E->Paircandidate();

  if (Z->type() == 22) {
  
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPt"]  -> Fill(Z->photon1()->pt()/1.e3, weight);  
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPt"]  -> Fill(Z->photon2()->pt()/1.e3, weight);    
    
    hm[CurrentShift + Analysis + HM_name + "h_ElectronEta"] -> Fill(Z->photon1()->eta(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ElectronEta"] -> Fill(Z->photon2()->eta(),     weight); 
    
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPhi"] -> Fill(Z->photon1()->phi(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPhi"] -> Fill(Z->photon2()->phi(),     weight); 
   
    
    hm[CurrentShift + Analysis + HM_name + "h_PhotonF1"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::f1),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonF1"] -> Fill(Z->photon2()->showerShapeValue(xAOD::EgammaParameters::f1),     weight);

    hm[CurrentShift + Analysis + HM_name + "h_PhotonReta"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::Reta),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonReta"] -> Fill(Z->photon2()->showerShapeValue(xAOD::EgammaParameters::Reta),     weight);

    hm[CurrentShift + Analysis + HM_name + "h_PhotonWeta2"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::weta2),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonWeta2"] -> Fill(Z->photon2()->showerShapeValue(xAOD::EgammaParameters::weta2),     weight);

    hm[CurrentShift + Analysis + HM_name + "h_PhotonEratio"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::Eratio),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonEratio"] -> Fill(Z->photon2()->showerShapeValue(xAOD::EgammaParameters::Eratio),     weight);

    hm[CurrentShift + Analysis + HM_name + "h_PhotonRhad"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::Rhad),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonRhad"] -> Fill(Z->photon2()->showerShapeValue(xAOD::EgammaParameters::Rhad),     weight);

    hm[CurrentShift + Analysis + HM_name + "h_PhotonRhad1"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::Rhad1),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonRhad1"] -> Fill(Z->photon2()->showerShapeValue(xAOD::EgammaParameters::Rhad1),     weight);



    hm[CurrentShift + Analysis + HM_name + "h_PhotonConv"] -> Fill(Z->photon1()->conversionType(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonConv"] -> Fill(Z->photon2()->conversionType(),     weight);
     
  }


  if (Z->type() == 1122) {
  
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPt"]  -> Fill(Z->photon1()->pt()/1.e3, weight);  
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPt"]  -> Fill(Z->electron1()->pt()/1.e3, weight);    
    
    hm[CurrentShift + Analysis + HM_name + "h_ElectronEta"] -> Fill(Z->photon1()->eta(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ElectronEta"] -> Fill(Z->electron1()->eta(),     weight); 
    
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPhi"] -> Fill(Z->photon1()->phi(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPhi"] -> Fill(Z->electron1()->phi(),     weight); 
    
    hm[CurrentShift + Analysis + HM_name + "h_PhotonF1"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::f1),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonReta"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::Reta),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonWeta2"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::weta2),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonEratio"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::Eratio),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonRhad"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::Rhad),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_PhotonRhad1"] -> Fill(Z->photon1()->showerShapeValue(xAOD::EgammaParameters::Rhad1),     weight); 
 
  }

  
  if (Z->type() == 11 || Z->type() == 1111) {
  
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPt"]  -> Fill(Z->electron1()->pt()/1.e3, weight);  
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPt"]  -> Fill(Z->electron2()->pt()/1.e3, weight);    
    
    hm[CurrentShift + Analysis + HM_name + "h_ElectronEta"] -> Fill(Z->electron1()->eta(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ElectronEta"] -> Fill(Z->electron2()->eta(),     weight); 
    
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPhi"] -> Fill(Z->electron1()->phi(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPhi"] -> Fill(Z->electron2()->phi(),     weight); 
    
    const xAOD::TrackParticle* tp1 = (Z->electron1())->trackParticle();
    const xAOD::TrackParticle* tp2 = (Z->electron2())->trackParticle();
    uint8_t pix1;
    tp1->summaryValue(pix1,xAOD::numberOfPixelHits);
    uint8_t pix2;
    tp2->summaryValue(pix2,xAOD::numberOfPixelHits);    
    
    hm[CurrentShift + Analysis + HM_name + "h_ElectronNPix"] -> Fill(pix1,     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ElectronNPix"] -> Fill(pix2,     weight); 
    
    float cl1 = Z->electron1()->caloCluster()->pt()/1.e3;
    float cl2 = Z->electron2()->caloCluster()->pt()/1.e3;
    
    if (cl1 > 3. && cl1 < 4. && cl2 > 3. && cl2 < 4. ) hm[CurrentShift + Analysis + HM_name + "h_ElectronDEt34"] -> Fill( (cl1-cl2)/((cl1+cl2)/2.), weight);
    if (cl1 > 4. && cl1 < 5. && cl2 > 4. && cl2 < 5. ) hm[CurrentShift + Analysis + HM_name + "h_ElectronDEt45"] -> Fill( (cl1-cl2)/((cl1+cl2)/2.), weight);
    if (cl1 > 5. && cl1 < 7. && cl2 > 5. && cl2 < 7. ) hm[CurrentShift + Analysis + HM_name + "h_ElectronDEt57"] -> Fill( (cl1-cl2)/((cl1+cl2)/2.), weight);
    if (cl1 > 7. && cl1 < 10. && cl2 > 7. && cl2 < 10. ) hm[CurrentShift + Analysis + HM_name + "h_ElectronDEt710"] -> Fill( (cl1-cl2)/((cl1+cl2)/2.), weight);
    if (cl1 > 10. && cl1 < 15. && cl2 > 10. && cl2 < 15. ) hm[CurrentShift + Analysis + HM_name + "h_ElectronDEt1015"] -> Fill( (cl1-cl2)/((cl1+cl2)/2.), weight);

    
    //hm[CurrentShift + Analysis + HM_name + "h_Electrond0"] -> Fill(Z->electron1()->trackParticle()->d0(),     weight); 
    //hm[CurrentShift + Analysis + HM_name + "h_Electrond0"] -> Fill(Z->electron2()->trackParticle()->d0(),     weight);  
  
    //hm[CurrentShift + Analysis + HM_name + "h_Electronz0"] -> Fill( ( Z->electron1()->trackParticle()->z0() //- 
    //  /*Z->electron1()->trackParticle()->vertex()->z()*/),     weight); 
    //hm[CurrentShift + Analysis + HM_name + "h_Electronz0"] -> Fill( ( Z->electron2()->trackParticle()->z0() //- 
    //  /*Z->electron2()->trackParticle()->vertex()->z()*/),     weight); 
  }

  if (Z->type() == -1113 || Z->type() == 1311) {
  
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPt"]  -> Fill(Z->electron1()->pt()/1.e3, weight);    
    
    hm[CurrentShift + Analysis + HM_name + "h_ElectronEta"] -> Fill(Z->electron1()->eta(),     weight);  
    
    hm[CurrentShift + Analysis + HM_name + "h_ElectronPhi"] -> Fill(Z->electron1()->phi(),     weight); 

  }  
 
  
}

