// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

// Handle error messages
#include "xAODRootAccess/tools/Message.h"

// EDM container 
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthEventContainer.h"

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

//#include <EXCLRunII/EXCLUserParticle.h>
#include <EXCLRunII/EXCLxAODTruAnalysis.h>

#include <iostream>
#include <string>

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
  do {                                                      \
  if( ! EXP.isSuccess() ) {                                 \
  Error( CONTEXT,                                           \
    XAOD_MESSAGE( "Failed to execute: %s" ),                \
	 #EXP );                                            \
  return EL::StatusCode::FAILURE;                           \
  }                                                         \
  } while( false )


// this is needed to distribute the algorithm to the workers
//ClassImp(EXCLxAODTruAnalysis)


// m_genMuon(0,0,0,0) to choose the right constructor
EXCLxAODTruAnalysis :: EXCLxAODTruAnalysis ()
//: m_genMuon(0,0,0,0)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

  // The following is important not to have a crash when delete  

  m_genZ = NULL;
  m_genW = NULL;

  m_genWMuonDRESS = NULL;
  m_genZMuon1DRESS = NULL;
  m_genZMuon2DRESS = NULL;
  m_genWNeutrinoMu = NULL;

  m_genWElectronDRESS = NULL;
  m_genZElectron1DRESS = NULL;
  m_genZElectron2DRESS = NULL;
  m_genWNeutrinoEle = NULL;
}


EL::StatusCode EXCLxAODTruAnalysis :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD ();
  //xAOD::Init(); // call before opening first file
  // Better use the error check 
  EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode EXCLxAODTruAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode EXCLxAODTruAnalysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode EXCLxAODTruAnalysis :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode EXCLxAODTruAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  m_eventCounter = 0;

  xAOD::TEvent* event = wk()->xaodEvent();
  // NuUmber of events in our xAOD
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode EXCLxAODTruAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  xAOD::TEvent* event = wk()->xaodEvent(); //to connect to the xAOD event store
  if(m_eventCounter < 6 || m_eventCounter % 200 == 0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++; 

  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("execute", event->retrieve( eventInfo, "EventInfo"));

  // check if the event is data or MC 
  bool isMC = false;
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = true;
  }
  if(isMC)
    {
      // TODO Get MC weights
      // Check if it is PHP or SHerpa etc .. 
      
      EXCLTruIsoTau();       // Check if IsoTau [GetEventTrueType()=15 ].  OK for all MC 
      //std::cout << " Event Type " << GetEventTrueType()  << std:: endl;

      EXCLTruEleMu_BuildIn(); // Relevant only for PHP, MC@NLO 
      if( FidRegionPtEtaLept() ) std::cout << " Pass Fiducial on Lepton Pt and Eta " << std::endl;
     }
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode EXCLxAODTruAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode EXCLxAODTruAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  xAOD::TEvent* event = wk()->xaodEvent();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode EXCLxAODTruAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
EL::StatusCode EXCLxAODTruAnalysis :: EXCLTruEleMu_BuildIn()
{
  //------------------------------------------------------------------
  // This routine gets the four 'BUILD IN' leptons from the decay of the W and Z
  //-----------------------------------------------------------------
  xAOD::TEvent* event = wk()->xaodEvent();

  if(m_genZ) delete m_genZ;
  if(m_genW) delete m_genW;
  m_genZ = NULL;
  m_genW = NULL;

  if( m_genWMuonDRESS )  delete m_genWMuonDRESS  ;
  if( m_genZMuon1DRESS ) delete m_genZMuon1DRESS ;
  if( m_genZMuon2DRESS ) delete m_genZMuon2DRESS ;
  if( m_genWNeutrinoMu ) delete m_genWNeutrinoMu ;

  if( m_genWElectronDRESS )  delete  m_genWElectronDRESS   ;
  if( m_genZElectron1DRESS ) delete  m_genZElectron1DRESS  ;
  if( m_genZElectron2DRESS ) delete m_genZElectron2DRESS ;
  if( m_genWNeutrinoEle )    delete  m_genWNeutrinoEle     ;

  m_genWMuonDRESS = NULL;
  m_genZMuon1DRESS = NULL;
  m_genZMuon2DRESS = NULL;  
  m_genWNeutrinoMu = NULL;

  m_genWElectronDRESS = NULL;
  m_genZElectron1DRESS = NULL;
  m_genZElectron2DRESS = NULL;
  m_genWNeutrinoEle = NULL;  

  std::cout << " -------------- In Buildin " << std::endl ;

  //--------------------------
  // Loop on Particle container to find W and Z
  //--------------------------

  //if PHP + P
  uint EXCLfinal_status = 62;
  // uint Zfinal_status = 155;   

  uint ZpdgId = 23;
  uint WpdgId = 24; 
  uint ntrueZ = 0;
  uint ntrueW = 0;

  const xAOD::TruthEventContainer* xTruthEventContainer = NULL;
  EL_RETURN_CHECK("EXCLTruEleMu_BuildIn",event->retrieve( xTruthEventContainer, "TruthEvents"));

  xAOD::TruthEventContainer::const_iterator tru_itr = xTruthEventContainer->begin();
  xAOD::TruthEventContainer::const_iterator tru_end = xTruthEventContainer->end();
  for( ; tru_itr != tru_end; ++tru_itr ) 
  {
    int nPart = (*tru_itr)->nTruthParticles();
    for(int iPart = 0;iPart<nPart;iPart++)
    {
      const xAOD::TruthParticle* particle = (*tru_itr)->truthParticle(iPart);
      if(particle)
	{
	  int pdgId = particle->pdgId();
	  uint status = particle->status();

          float part_pt  = particle->pt()/1000; 
          float part_e   = particle->pt()/1000;
	  float part_rapidity = particle->rapidity();
	  float part_phi = particle->phi();
                    
          if(fabs(pdgId) == ZpdgId && status == EXCLfinal_status ) 
	    {
	      ntrueZ++;
              if(ntrueZ == 1)  m_genZ = new EXCLUserParticle (0, 1, part_e, part_pt, part_rapidity, part_phi );
            }
	  if(fabs(pdgId) == WpdgId && status == EXCLfinal_status ) 
	    {
              ntrueW++;
              if(ntrueW == 1) m_genW = new EXCLUserParticle (0, 1, part_e, part_pt, part_rapidity, part_phi );
	    }
	  if (fabs(pdgId) == ZpdgId || fabs(pdgId) == WpdgId ) PrintIDStatusPart(particle);
        } // if particle
    }// if  particle
  } // loop on particle
 //---------------------------
  // Loop on True Photons
  //---------------------------  
  uint ntruephoton_gen=0;   
  uint ntruephotonW_gen=0;  
  uint ntruephotonZ_gen=0;
  uint ntruephoton_iso=0;

  short g_chargeW =0;
  short g_charge1 = 0;

  const xAOD::TruthParticleContainer* tphotons = 0;
  EL_RETURN_CHECK("EXCLTruEleMu_BuildIn", event->retrieve( tphotons, "TruthPhotons" ));
  xAOD::TruthParticleContainer::const_iterator tphotons_itr = tphotons->begin();
  xAOD::TruthParticleContainer::const_iterator tphotons_end = tphotons->end();

  for (; tphotons_itr != tphotons_end; ++tphotons_itr)
    {
      //      uint g_type = (*tphotons_itr)->auxdata<uint>("particleType");
      // uint g_orig =  (*tphotons_itr)->auxdata<uint>("particleOrigin");
      uint g_type = (*tphotons_itr)->auxdata<uint>("classifierParticleType"); 
      uint g_orig =  (*tphotons_itr)->auxdata<uint>("classifierParticleOrigin"); 

      float g_pt = (*tphotons_itr)->auxdata<float>("pt_dressed");
      float g_e = (*tphotons_itr)->auxdata<float>("e_dressed");
      float g_eta = (*tphotons_itr)->auxdata<float>("eta_dressed");
      float g_phi = (*tphotons_itr)->auxdata<float>("phi_dressed");

      int g_pdgId  =   (*tphotons_itr)->pdgId();
      short g_charge = -1*g_pdgId/fabs(g_pdgId);
      uint g_status =  (*tphotons_itr)->status();
      int g_barcode = (*tphotons_itr)->barcode();
      //int mother = (*tphotons_itr)->mother(0)->pdgId();

      //float g_naked_pt = (*tphotons_itr)->pt()/1000;
      //float g_naked_e =  (*tphotons_itr)->e()/1000;
      //float g_naked_eta =  (*tphotons_itr)->eta();
      //float g_naked_phi =  (*tphotons_itr)->phi();

      bool hasProdVtx = (*tphotons_itr)->hasProdVtx();
      if(hasProdVtx) 
	{
           const xAOD::TruthVertex* Vtx_Par = (*tphotons_itr)->prodVtx();
	   if(Vtx_Par) {
	             float vtx_x = Vtx_Par->x();
		     std::cout << " vtx " << vtx_x  << std::endl;
	             // int NN =  prodVtx->numIncomingParticles();
    	     // const xAOD::TruthParticle* parent = Vtx_Par()->incomingParticle();
	   }
	}
      if( fabs(g_pdgId) == 13 && g_status == 1 && g_barcode < 200000)
	{
	  ntruephoton_gen++;
	  if(g_type == 6) ntruephoton_iso++;
	  if(g_type == 6 && g_orig == 12)
	    { 
              ntruephotonW_gen++;
                             
              if(ntruephotonW_gen == 1 )  { 
		    g_chargeW = g_charge;
                    m_genWPhotonDRESS = new EXCLUserParticle (0, g_charge, g_e, g_pt, g_eta, g_phi );
		// Here I can use directely the function of the class
		//m_genWPhoton.Set 
	      std::cout << " In Build in Pt= " << g_pt <<  std::endl;}
	    }
          if(g_type == 6 && g_orig == 13) 
            {
               ntruephotonZ_gen++;
	       if(ntruephotonZ_gen == 1 )  
		 {  g_charge1 = g_charge;
                    m_genZPhoton1DRESS = new EXCLUserParticle (0, g_charge, g_e, g_pt, g_eta, g_phi );
                 }
	       if(ntruephotonZ_gen == 2 && g_charge*g_charge1 < 0 )  m_genZPhoton2DRESS = new EXCLUserParticle (0, g_charge, g_e, g_pt, g_eta, g_phi );
	     }

          PrintOrigtExtractedPart(  g_pdgId, g_status,  g_barcode, g_type, g_orig );
	  // photons_type.push_back(g_type);
	  // photons_origin.push_back(g_orig);

	  // vgDRESS.SetPtEtaPhiE(g_pt, g_eta, g_phi, g_e);
	  // photonDRESS.push_back(vgDRESS);
	  // vgBARE.SetPtEtaPhiE(g_naked_pt, g_naked_eta, g_naked_phi, g_naked_e);
	  // photonBARE.push_back(vgBARE);
	}
    } // End Loop on Photons
  //---------------------------
  // Loop on True Muons
  //---------------------------  
  uint ntruemuon_gen=0;   
  uint ntruemuonW_gen=0;  
  uint ntruemuonZ_gen=0;
  uint ntruemuon_iso=0;

  short mu_chargeW =0;
  short mu_charge1 = 0;

  const xAOD::TruthParticleContainer* tmuons = 0;
  EL_RETURN_CHECK("EXCLTruEleMu_BuildIn", event->retrieve( tmuons, "TruthMuons" ));
  xAOD::TruthParticleContainer::const_iterator tmuons_itr = tmuons->begin();
  xAOD::TruthParticleContainer::const_iterator tmuons_end = tmuons->end();

  for (; tmuons_itr != tmuons_end; ++tmuons_itr)
    {
      //      uint mu_type = (*tmuons_itr)->auxdata<uint>("particleType");
      // uint mu_orig =  (*tmuons_itr)->auxdata<uint>("particleOrigin");
      uint mu_type = (*tmuons_itr)->auxdata<uint>("classifierParticleType"); 
      uint mu_orig =  (*tmuons_itr)->auxdata<uint>("classifierParticleOrigin"); 

      float mu_pt = (*tmuons_itr)->auxdata<float>("pt_dressed");
      float mu_e = (*tmuons_itr)->auxdata<float>("e_dressed");
      float mu_eta = (*tmuons_itr)->auxdata<float>("eta_dressed");
      float mu_phi = (*tmuons_itr)->auxdata<float>("phi_dressed");

      int mu_pdgId  =   (*tmuons_itr)->pdgId();
      short mu_charge = -1*mu_pdgId/fabs(mu_pdgId);
      uint mu_status =  (*tmuons_itr)->status();
      int mu_barcode = (*tmuons_itr)->barcode();
      //int mother = (*tmuons_itr)->mother(0)->pdgId();

      //float mu_naked_pt = (*tmuons_itr)->pt()/1000;
      //float mu_naked_e =  (*tmuons_itr)->e()/1000;
      //float mu_naked_eta =  (*tmuons_itr)->eta();
      //float mu_naked_phi =  (*tmuons_itr)->phi();

      bool hasProdVtx = (*tmuons_itr)->hasProdVtx();
      if(hasProdVtx) 
	{
           const xAOD::TruthVertex* Vtx_Par = (*tmuons_itr)->prodVtx();
	   if(Vtx_Par) {
	             float vtx_x = Vtx_Par->x();
		     std::cout << " vtx " << vtx_x  << std::endl;
	             // int NN =  prodVtx->numIncomingParticles();
    	     // const xAOD::TruthParticle* parent = Vtx_Par()->incomingParticle();
	   }
	}
      if( fabs(mu_pdgId) == 13 && mu_status == 1 && mu_barcode < 200000)
	{
	  ntruemuon_gen++;
	  if(mu_type == 6) ntruemuon_iso++;
	  if(mu_type == 6 && mu_orig == 12)
	    { 
              ntruemuonW_gen++;
                             
              if(ntruemuonW_gen == 1 )  { 
		    mu_chargeW = mu_charge;
                    m_genWMuonDRESS = new EXCLUserParticle (0, mu_charge, mu_e, mu_pt, mu_eta, mu_phi );
		// Here I can use directely the function of the class
		//m_genWMuon.Set 
	      std::cout << " In Build in Pt= " << mu_pt <<  std::endl;}
	    }
          if(mu_type == 6 && mu_orig == 13) 
            {
               ntruemuonZ_gen++;
	       if(ntruemuonZ_gen == 1 )  
		 {  mu_charge1 = mu_charge;
                    m_genZMuon1DRESS = new EXCLUserParticle (0, mu_charge, mu_e, mu_pt, mu_eta, mu_phi );
                 }
	       if(ntruemuonZ_gen == 2 && mu_charge*mu_charge1 < 0 )  m_genZMuon2DRESS = new EXCLUserParticle (0, mu_charge, mu_e, mu_pt, mu_eta, mu_phi );
	     }

          PrintOrigtExtractedPart(  mu_pdgId, mu_status,  mu_barcode, mu_type, mu_orig );
	  // muons_type.push_back(mu_type);
	  // muons_origin.push_back(mu_orig);

	  // vmuDRESS.SetPtEtaPhiE(mu_pt, mu_eta, mu_phi, mu_e);
	  // muonDRESS.push_back(vmuDRESS);
	  // vmuBARE.SetPtEtaPhiE(mu_naked_pt, mu_naked_eta, mu_naked_phi, mu_naked_e);
	  // muonBARE.push_back(vmuBARE);
	}
    } // End Loop on Muons

  //----------------------------------------------------------
  //Loop on True  Electrons
  //---------------------------------------------------------
  uint ntruelectron_gen     =0;
  uint ntruelectron_iso  =0;
  uint ntruelectronW_gen   =0;
  uint ntruelectronZ_gen   =0;
  short el_chargeW =0;
  short el_charge1 = 0;
   
  const xAOD::TruthParticleContainer* telecs = 0;
  EL_RETURN_CHECK("EXCLTruEleMu_BuildIn", event->retrieve( telecs, "TruthElectrons" ));
  xAOD::TruthParticleContainer::const_iterator telecs_itr = telecs->begin();
  xAOD::TruthParticleContainer::const_iterator telecs_end = telecs->end();
 
  for (; telecs_itr != telecs_end; ++telecs_itr)
    {
      uint el_type = (*telecs_itr)->auxdata<uint>("classifierParticleType");
      uint el_orig =  (*telecs_itr)->auxdata<uint>("classifierParticleOrigin");

      float el_pt = (*telecs_itr)->auxdata<float>("pt_dressed")/1000;
      float el_e = (*telecs_itr)->auxdata<float>("e_dressed")/1000;
      float el_eta = (*telecs_itr)->auxdata<float>("eta_dressed");
      float el_phi = (*telecs_itr)->auxdata<float>("phi_dressed");

      int   el_pdgId  =   (*telecs_itr)->pdgId();
      short el_charge = -1* el_pdgId/fabs(el_pdgId);
      uint  el_status =  (*telecs_itr)->status();
      uint  el_barcode = (*telecs_itr)->barcode();

      //float el_naked_pt = (*telecs_itr)->pt()/1000;
      //float el_naked_e =  (*telecs_itr)->e()/1000;
      //float el_naked_eta =  (*telecs_itr)->eta();
      //float el_naked_phi =  (*telecs_itr)->phi();

      if( fabs(el_pdgId) == 11 && el_status == 1 && el_barcode < 200000)
	{
	  ntruelectron_gen++;
          if(el_type == 2 ) ntruelectron_iso++;
	  if (el_type == 2 && el_orig == 12)
	    {
	      ntruelectronW_gen++;
	    std::cout <<"ntruelectronW_gen" << ntruelectronW_gen<<std::endl;
	      if(ntruelectronW_gen == 1 ) {  
                   m_genWElectronDRESS = new EXCLUserParticle (0, el_charge, el_e, el_pt, el_eta, el_phi );
                   el_chargeW = el_charge;
	      }
	    }
	   if (el_type == 2 && el_orig == 13)
	    {
              ntruelectronZ_gen++;  
	      if(ntruelectronZ_gen == 1 )
		{
		  el_charge1 = el_charge; 
                  m_genZElectron1DRESS = new EXCLUserParticle (0, el_charge, el_e, el_pt, el_eta, el_phi );
		}
	      if(ntruelectronZ_gen == 2 && el_charge *el_charge1 )  m_genZElectron2DRESS = new EXCLUserParticle (0, el_charge, el_e, el_pt, el_eta, el_phi );
            } // Electrons from Z
	} // Final state generated True Electrons

      PrintOrigtExtractedPart(  el_pdgId, el_status,  el_barcode, el_type, el_orig );
    }// End loop on True Electrons
  //------------------------------------
  //Loop on True  Neutrinos  
  //---------------------------------------------------------
  uint  ntrueNeutrino = 0;
  uint  ntrueNeutrinoMuW_gen =0;
  uint  ntrueNeutrinoEleW_gen  =0;
  const xAOD::TruthParticleContainer* tneutrinos = 0;
  EL_RETURN_CHECK("EXCLTruEleMu_BuildIne", event->retrieve( tneutrinos, "TruthNeutrinos" ));
  xAOD::TruthParticleContainer::const_iterator tneutrinos_itr = tneutrinos->begin();
  xAOD::TruthParticleContainer::const_iterator tneutrinos_end = tneutrinos->end(); 

  for (; tneutrinos_itr != tneutrinos_end; ++tneutrinos_itr)
    {
      uint nu_type = (*tneutrinos_itr)->auxdata<uint>("classifierParticleType");
      uint nu_orig = (*tneutrinos_itr)->auxdata<uint>("classifierParticleOrigin");

      //float nu_pt =  (*tneutrinos_itr)->auxdata<float>("pt_dressed")/1000;
      //float nu_e =   (*tneutrinos_itr)->auxdata<float>("e_dressed")/1000;
      //float nu_eta = (*tneutrinos_itr)->auxdata<float>("eta_dressed");
      // float nu_phi = (*tneutrinos_itr)->auxdata<float>("phi_dressed");

      float nu_pt =  (*tneutrinos_itr)->pt()/1000; 
      float nu_e =   (*tneutrinos_itr)->e()/1000;
      float nu_eta = (*tneutrinos_itr)->eta();
      float nu_phi = (*tneutrinos_itr)->phi();

      int  nu_pdgId  =  (*tneutrinos_itr)->pdgId();
      short nu_kind  = -1 * nu_pdgId/fabs(nu_pdgId);
      uint nu_status =  (*tneutrinos_itr)->status();
      uint nu_barcode = (*tneutrinos_itr)->barcode();

      if(nu_type == 18 ) ntrueNeutrino ++;
      if(nu_type == 18 && nu_orig == 12 )
	{
        if( fabs(nu_pdgId) == 13 && nu_status == 1 && nu_barcode < 200000)
          {
	     ntrueNeutrinoMuW_gen++;
             if(ntrueNeutrinoMuW_gen == 1 && nu_kind*mu_chargeW < 0 ) m_genWNeutrinoMu = new EXCLUserParticle (0, 0, nu_e, nu_pt, nu_eta, nu_phi );
          } // nu_mu
      if( fabs(nu_pdgId) == 12 && nu_status == 1 && nu_barcode < 200000)
	{
	    ntrueNeutrinoEleW_gen++;
            if(ntrueNeutrinoMuW_gen == 1 && nu_kind * el_chargeW < 0 ) m_genWNeutrinoEle = new EXCLUserParticle (0, 0, nu_e, nu_pt, nu_eta, nu_phi );
          }
	} // nu_ele
      PrintOrigtExtractedPart(  nu_pdgId, nu_status,  nu_barcode, nu_type, nu_orig );
    } // Loop on neutrinos

  uint xtypev = 99;

  bool xtypevZee  = false; 
  bool xtypevWenu = false;
  bool xtypevZmumu = false;  
  bool xtypevWmunu = false;
 
  if( ntrueZ == 1 && ntrueW == 1 ) xtypev = 20;

  if( ntruelectronZ_gen == 2 ) xtypevZee = true;
  if( ntruelectronW_gen == 1 && ntrueNeutrinoEleW_gen == 1 )  xtypevWenu = true;

  if(ntruemuonZ_gen    == 2) xtypevZmumu = true;
  if(ntruemuonW_gen    == 1 && ntrueNeutrinoMuW_gen  == 1 ) xtypevWmunu = true;

  // Diboson EXCL
  if(  xtypevZee && xtypevWenu  && !xtypevZmumu  && !xtypevWmunu ) xtypev = 1; // eee
  if(  xtypevZee && !xtypevWenu && !xtypevZmumu  &&  xtypevWmunu ) xtypev = 2; // eemu 
  if( !xtypevZee &&  xtypevWenu &&  xtypevZmumu  && !xtypevWmunu ) xtypev = 3; // mumue
  if( !xtypevZee && !xtypevWenu &&  xtypevZmumu  &&  xtypevWmunu ) xtypev = 4; // mumumu

  // Single Boson
  if(  !xtypevZee && xtypevWenu  && !xtypevZmumu  && !xtypevWmunu ) xtypev = 10; // enu  
  if(  !xtypevZee && !xtypevWenu  && !xtypevZmumu  && xtypevWmunu ) xtypev = 11; // munu  

  if(  xtypevZee && !xtypevWenu  && !xtypevZmumu  && !xtypevWmunu ) xtypev = 13; // ee
  if(  !xtypevZee && !xtypevWenu && xtypevZmumu   && !xtypevWmunu ) xtypev = 14; // mumu 
  
  SetEventTrueType(xtypev);

 return EL::StatusCode::SUCCESS;
}
EL::StatusCode EXCLxAODTruAnalysis :: EXCLTruIsoTau()
{
  ///////////////////////////////////////////////////////////////////////////
  //  This is to check and eventally discard the taus "not from hadron decay" 
  //////////////////////////////////////////////////////////////////////////
  xAOD::TEvent* event = wk()->xaodEvent();
 
  std::cout << " -------------- Iso Tau " << std::endl ;
  //----------------------------------------------------------
  // Loop on taus
  //----------------------------------------------------------
  uint ntruetau_iso     =0;
  const xAOD::TruthParticleContainer* ttaus = 0; 
  EL_RETURN_CHECK("EXCLTruIsoTau", event->retrieve( ttaus, "TruthTaus" ));
 
  xAOD::TruthParticleContainer::const_iterator ttaus_itr =  ttaus->begin();
  xAOD::TruthParticleContainer::const_iterator ttaus_end = ttaus->end();

  for (; ttaus_itr != ttaus_end; ++ttaus_itr)
    {
      uint tau_type = (*ttaus_itr)->auxdata<uint>("classifierParticleType");
      uint tau_orig = (*ttaus_itr)->auxdata<uint>("classifierParticleOrigin");

      int   tau_pdgId  =  (*ttaus_itr)->pdgId();

      uint  tau_status =  (*ttaus_itr)->status();
      uint  tau_barcode = (*ttaus_itr)->barcode();
      PrintOrigtExtractedPart(  tau_pdgId, tau_status,  tau_barcode, tau_type, tau_orig );

      if (tau_type == 10 ) 
	{// IsoTau (not from HF hadrons, hopefully?) 
	  ntruetau_iso++;
         std::cout << " The event has at least a true tau " << std::endl;
	}
    } // Loop on Taus
  if( ntruetau_iso == 0 ) SetEventTrueType(15);
  return EL::StatusCode::SUCCESS;
 }
//==================================================================
//=================================================================
void EXCLxAODTruAnalysis :: PrintIDStatusPart(const xAOD::TruthParticle* tx)
{
  std::cout << " ID= " << tx->pdgId()  << " Status= "  << tx->status() << " Barcode= "<< tx->barcode()  << std::endl;
}
//================================================================
//==============================================================
void EXCLxAODTruAnalysis :: PrintOrigtExtractedPart( int  us_pdgId, uint  us_status,  uint  us_barcode, uint us_type, uint us_orig )
{
  std::cout <<
  " ID= " << us_pdgId  << " Status= "  << us_status << " Barcode= "<< us_barcode  << " TYPE="<< us_type  << " ORIG= " << us_orig <<  std::endl;
  // std::cout << " -------------------------------------------------------------" << std::endl;
}
//===============================================================
//==============================================================
void EXCLxAODTruAnalysis :: EXCLTruEleMu_RS()
{
}
bool EXCLxAODTruAnalysis :: FidRegionPtEtaLept()
{
  bool ipass    = false;
  bool ipassPt  = false;
  bool ipassEta = false;
  
  std:: cout << " I am in Fiducial "  << GetEventTrueType() << std::endl;

  float PtWLepCut = 20;
  float PtZLep1Cut = 15;
  float PtZLep2Cut = 15;

  float EtaWLepCut  = 2.5;
  float EtaZLep1Cut = 2.5;
  float EtaZLep2Cut = 2.5;

  float PtWLep  = 0;
  float PtZLep1 = 0;
  float PtZLep2  = 0;

  float EtaWLep   = 10; 
  float EtaZLep1  = 10; 
  float EtaZLep2  = 10; 
 
  if(GetEventTrueType() > 0 && GetEventTrueType() < 5 )
    {

      if(m_genWElectronDRESS && m_genZElectron1DRESS && m_genZElectron2DRESS)
	{//eee
           PtWLep  = m_genWElectronDRESS->Pt();
           PtZLep1 = m_genZElectron1DRESS->Pt();
           PtZLep2  = m_genZElectron2DRESS->Pt();

           EtaWLep    =  m_genWElectronDRESS->Eta();
           EtaZLep1   =  m_genZElectron1DRESS->Eta();
	   EtaZLep2  =   m_genZElectron1DRESS->Eta();
        }

      if( m_genWMuonDRESS && m_genZElectron1DRESS && m_genZElectron2DRESS)
	{//eemu
           PtWLep  =  m_genWMuonDRESS->Pt();
           PtZLep1 = m_genZElectron1DRESS->Pt();
           PtZLep2  = m_genZElectron2DRESS->Pt();

           EtaWLep    =   m_genWMuonDRESS->Eta();
           EtaZLep1   =  m_genZElectron1DRESS->Eta();
           EtaZLep2  =   m_genZElectron1DRESS->Eta();
        }
      if(m_genWElectronDRESS && m_genZMuon1DRESS && m_genZMuon2DRESS)
	{//mumue
           PtWLep  = m_genWElectronDRESS->Pt();
           PtZLep1 = m_genZMuon1DRESS->Pt();
           PtZLep2 = m_genZMuon2DRESS->Pt();

           EtaWLep    =  m_genWElectronDRESS->Eta();
           EtaZLep1   =  m_genZMuon1DRESS->Eta();
           EtaZLep2  =   m_genZMuon1DRESS->Eta();
        }

      if(m_genWMuonDRESS && m_genZMuon1DRESS && m_genZMuon2DRESS)
	{ // mumumu
           PtWLep  = m_genWMuonDRESS->Pt();
           PtZLep1 = m_genZMuon1DRESS->Pt();
	   PtZLep2 = m_genZMuon2DRESS->Pt();

	   EtaWLep   =  m_genWMuonDRESS->Eta();
           EtaZLep1  =  m_genZMuon1DRESS->Eta();
           EtaZLep2  =  m_genZMuon2DRESS->Eta();
        }
    }
   
      if( PtWLep > PtWLepCut && PtZLep1 > PtZLep1Cut && PtZLep2 > PtZLep2Cut ) ipassPt  = true;
      if( fabs( EtaWLep) < EtaWLepCut && fabs( EtaZLep1) < EtaZLep1Cut && fabs (EtaZLep2) < EtaZLep2Cut ) ipassEta = true; 
      if(ipassPt && ipassEta) ipass = true;
    
  return ipass;
}
