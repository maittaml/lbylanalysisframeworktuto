#include "EXCLRunII/ExclVeto.h"


bool ExclVetoCandidate::passedRandomSampling(double z_ref, double vetoSize)  const{

   double rndmvx_z0 = gRandom->Gaus(-5.98588e+00, 4.42579e+01);

   
   while( fabs( rndmvx_z0  - z_ref )<30.) 
   {
   	rndmvx_z0 = gRandom->Gaus(-5.98588e+00, 4.42579e+01);
   }
   
   
   for(unsigned int trki=0; trki < m_fullTrkList.size(); trki++)
   { 
    float trk_z0 = (m_fullTrkList.at(trki))->z0();
    float beamspot_z0 = (m_fullTrkList.at(trki))->vz();
    float theta = (m_fullTrkList.at(trki))->theta();
    if ( fabs((m_fullTrkList.at(trki))->d0()) > 1.5 ) continue;
      if(fabs((trk_z0 - rndmvx_z0 + beamspot_z0)/*sin(theta)*/) < 3.0) return false;
   }   
   
   return true;
}
