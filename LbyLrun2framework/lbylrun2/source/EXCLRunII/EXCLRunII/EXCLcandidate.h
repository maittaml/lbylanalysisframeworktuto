#ifndef EXCLCANDIDATE_H
#define EXCLCANDIDATE_H
 
#include "EXCLRunII/ExclVeto.h"
#include "EXCLRunII/Pair.h"


class EXCLCandidate {

  public:
   
    EXCLCandidate( int type, const PairCandidate* Paircandidate, const ExclVetoCandidate* ExclVetocandidate, int sum_pix ) :
		 m_type(type), m_Paircandidate(Paircandidate), m_ExclVetocandidate(ExclVetocandidate), m_sumpix(sum_pix) { }
		 
    EXCLCandidate() { }	 
    ~EXCLCandidate() { }

    int   type() const {return m_type; }
    const PairCandidate* Paircandidate() const { return m_Paircandidate; }
    const ExclVetoCandidate* ExclVetocandidate() const { return m_ExclVetocandidate; }  
    double vtxIsoSize( const xAOD::Vertex * m_Vtx/* = m_ExclVetocandidate->priVtx()*/ ); 
    int   sumpix() const {return m_sumpix; }
    
  private: 
  
    int m_type;
    const PairCandidate* m_Paircandidate;
    const ExclVetoCandidate* m_ExclVetocandidate;
    int m_sumpix;
      
};
  
#endif
