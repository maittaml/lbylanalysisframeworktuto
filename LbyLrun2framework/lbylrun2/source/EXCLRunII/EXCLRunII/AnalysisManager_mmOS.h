#ifndef ANALYSISMANAGER_MMOS_H
#define ANALYSISMANAGER_MMOS_H

#include <TH1.h>
#include <map>
#include "EXCLRunII/EXCLcandidate.h"
#include "EXCLRunII/ExclVeto.h"
#include "EXCLRunII/Pair.h"
#include "EXCLRunII/HistManagerPair.h"
#include "EXCLRunII/HistManagerTracking.h"  
#include "EXCLRunII/HistManagerMuon.h"  
#include "EXCLRunII/HistManagerElectron.h" 
#include "EXCLRunII/HistManagerEvent.h" 
#include "EXCLRunII/HistManagerEXCL.h"



class AnalysisManager_mmOS {

  public: 
  
  HistManagerPair    m_HistManagerPair;   //!
  HistManagerTracking  m_HistManagerTracking;   //!
  HistManagerMuon      m_HistManagerMuon;     //!  
  HistManagerElectron  m_HistManagerElectron; //! 
  HistManagerEvent     m_HistManagerEvent;    //!   
  HistManagerEXCL        m_HistManagerEXCL;       //! 
    
  TString Analysis = "/mumuOS";
   
  std::map<TString, TH1*> bookHistos(TString SystematicName);     //! books all histograms
  
  void fillHistos(const EXCLCandidate* E, const xAOD::EventInfo* eventInfo, float weight, std::map<TString, TH1*> Histogram_map, TString SystematicName); //! 
     
};

#endif
