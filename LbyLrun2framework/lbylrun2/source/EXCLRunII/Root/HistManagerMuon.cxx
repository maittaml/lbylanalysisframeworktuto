#include "EXCLRunII/HistManagerMuon.h"

std::map<TString, TH1*> HistManagerMuon :: bookHistos(TString CurrentShift, TString Analysis) {
  
  //Book all histograms here

  std::map<TString, TH1*> hm;
  hm.clear();

  TH1* hMuonPt   = new TH1F(CurrentShift + Analysis + HM_name + "h_MuonPt",	"MuonPt",   400,    0., 200.); 
  hm[hMuonPt->GetName()] = hMuonPt;  
    
  TH1* hMuonEta  = new TH1F(CurrentShift + Analysis + HM_name + "h_MuonEta",	"MuonEta",  60.,  -3.0, 3.0); 
  hm[hMuonEta->GetName()] = hMuonEta;

  TH1* hMuonLPt   = new TH1F(CurrentShift + Analysis + HM_name + "h_MuonLPt",	"MuonLPt",   100,    0., 100.); 
  hm[hMuonLPt->GetName()] = hMuonLPt; 
  TH1* hMuonSPt   = new TH1F(CurrentShift + Analysis + HM_name + "h_MuonSPt",	"MuonSPt",   100,    0., 100.); 
  hm[hMuonSPt->GetName()] = hMuonSPt; 

  TH1* hMuonLEta  = new TH1F(CurrentShift + Analysis + HM_name + "h_MuonLEta",	"MuonLEta",  60.,  -3.0, 3.0); 
  hm[hMuonLEta->GetName()] = hMuonLEta;
  TH1* hMuonSEta  = new TH1F(CurrentShift + Analysis + HM_name + "h_MuonSEta",	"MuonSEta",  60.,  -3.0, 3.0); 
  hm[hMuonSEta->GetName()] = hMuonSEta;

      
  TH1* hMuonPhi  = new TH1F(CurrentShift + Analysis + HM_name + "h_MuonPhi",	"MuonPhi",  64.,  -3.2, 3.2); 
  hm[hMuonPhi->GetName()] = hMuonPhi;  

  TH1* hMuond0  = new TH1F(CurrentShift + Analysis + HM_name + "h_Muond0",	"Muond0",  200.,  -1., 1.); 
  hm[hMuond0->GetName()] = hMuond0;

  
  return hm;
  
}

void HistManagerMuon :: fillHistos(const EXCLCandidate* E, float weight, std::map<TString, TH1*> hm, TString CurrentShift, TString Analysis) {

//  if (!EXCL) return;
  const PairCandidate *Z = E->Paircandidate();

  if (Z->type() == 13 || Z->type() == 1313) {
  
    hm[CurrentShift + Analysis + HM_name + "h_MuonPt"]  -> Fill(Z->muon1()->pt()/1.e3, weight); 
    hm[CurrentShift + Analysis + HM_name + "h_MuonPt"]  -> Fill(Z->muon2()->pt()/1.e3, weight);       
    
    hm[CurrentShift + Analysis + HM_name + "h_MuonEta"] -> Fill(Z->muon1()->eta(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_MuonEta"] -> Fill(Z->muon2()->eta(),     weight); 
    
    hm[CurrentShift + Analysis + HM_name + "h_MuonPhi"] -> Fill(Z->muon1()->phi(),     weight); 
    hm[CurrentShift + Analysis + HM_name + "h_MuonPhi"] -> Fill(Z->muon2()->phi(),     weight); 
    
    if (Z->muon1()->pt() > Z->muon2()->pt()){

      hm[CurrentShift + Analysis + HM_name + "h_MuonLPt"]  -> Fill(Z->muon1()->pt()/1.e3, weight); 
      hm[CurrentShift + Analysis + HM_name + "h_MuonSPt"]  -> Fill(Z->muon2()->pt()/1.e3, weight);       
    
      hm[CurrentShift + Analysis + HM_name + "h_MuonLEta"] -> Fill(Z->muon1()->eta(),     weight); 
      hm[CurrentShift + Analysis + HM_name + "h_MuonSEta"] -> Fill(Z->muon2()->eta(),     weight);     
    
    }
    else{
    
      hm[CurrentShift + Analysis + HM_name + "h_MuonLPt"]  -> Fill(Z->muon2()->pt()/1.e3, weight); 
      hm[CurrentShift + Analysis + HM_name + "h_MuonSPt"]  -> Fill(Z->muon1()->pt()/1.e3, weight);       
    
      hm[CurrentShift + Analysis + HM_name + "h_MuonLEta"] -> Fill(Z->muon2()->eta(),     weight); 
      hm[CurrentShift + Analysis + HM_name + "h_MuonSEta"] -> Fill(Z->muon1()->eta(),     weight);      
    
    }

  }

  if (Z->type() == -1113 || Z->type() == 1311) {
  
    hm[CurrentShift + Analysis + HM_name + "h_MuonPt"]  -> Fill(Z->muon1()->pt()/1.e3, weight);     
    
    hm[CurrentShift + Analysis + HM_name + "h_MuonEta"] -> Fill(Z->muon1()->eta(),     weight);  
    
    hm[CurrentShift + Analysis + HM_name + "h_MuonPhi"] -> Fill(Z->muon1()->phi(),     weight);  
    
  }  

  
  
}

