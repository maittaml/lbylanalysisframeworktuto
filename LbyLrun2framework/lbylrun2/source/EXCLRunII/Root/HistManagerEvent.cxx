#include "EXCLRunII/HistManagerEvent.h"

std::map<TString, TH1*> HistManagerEvent :: bookHistos(TString CurrentShift, TString Analysis) {
//Book all histograms here

  std::map<TString, TH1*> hm;
  hm.clear();
  
  TH1* h = NULL; 
  
  h = new TH1F(CurrentShift + Analysis + HM_name + "ActualNInt",  "ActualInt",  50, 0., 50. );
  hm[h->GetName()] = h;    
  
  h = new TH1F(CurrentShift + Analysis + HM_name + "AverageNInt", "AverageInt", 50, 0., 50. );
  hm[h->GetName()] = h;   
  
  //h = new TH1F(CurrentShift + Analysis + HM_name + "Nvtx",        "Nvtx",       50, 0., 50. );
  //hm[h->GetName()] = h;     
  
  //add run number, event number EventInfo.runNumber() EventInfo.eventNumber()
  
  return hm;
}

void HistManagerEvent :: fillHistos(const xAOD::EventInfo* eventInfo, float weight, std::map<TString, TH1*> hm, TString CurrentShift, TString Analysis){

  hm[CurrentShift + Analysis + HM_name + "ActualNInt"]  -> Fill(eventInfo->actualInteractionsPerCrossing(),  weight); 

  hm[CurrentShift + Analysis + HM_name + "AverageNInt"] -> Fill(eventInfo->averageInteractionsPerCrossing(), weight); 

}
