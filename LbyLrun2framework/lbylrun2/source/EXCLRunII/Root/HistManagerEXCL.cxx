#include "EXCLRunII/HistManagerEXCL.h"

std::map<TString, TH1*> HistManagerEXCL :: bookHistos(TString CurrentShift, TString Analysis) {
//Book all histograms here

  std::map<TString, TH1*> hm;
  hm.clear();
  
  TH1* h = NULL; 
  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_DeltaY",     "NoSelection_h_DeltaY",    8,  0, 5); // |yZ-y_{l,W}|
  hm[h->GetName()] = h;  
  
  return hm;
}

void HistManagerEXCL :: fillHistos(const EXCLCandidate* E, float weight, std::map<TString, TH1*> hm, TString CurrentShift, TString Analysis){

  const PairCandidate *Z = E->Paircandidate();
  
  if (Z->type() == 13) {
    hm[CurrentShift + Analysis + HM_name + "h_DeltaY"] -> Fill( fabs( Z->fourMomentum().Rapidity()),     weight );    
  }
  
  if (Z->type() == 11) {
    //hm[CurrentShift + Analysis + HM_name + "h_DeltaY"] -> Fill( fabs( WZ->Zcandidate()->fourMomentum().Rapidity() - WZ->Wcandidate()->electron()->rapidity()), weight );  
    hm[CurrentShift + Analysis + HM_name + "h_DeltaY"] -> Fill( fabs( Z->fourMomentum().Rapidity() ), weight );       
  }  
}
