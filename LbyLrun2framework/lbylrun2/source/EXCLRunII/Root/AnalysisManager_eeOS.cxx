#include "EXCLRunII/AnalysisManager_eeOS.h"


std::map<TString, TH1*> AnalysisManager_eeOS :: bookHistos(TString SystematicName){

  std::map<TString, TH1*> hm;
  std::map<TString, TH1*> hm_f;  
  
  hm.clear();
  //m_HistManagerPair = new HistManagerPair();
  
  hm_f.clear();  
  hm_f = m_HistManagerPair.bookHistos(SystematicName, Analysis); //with some name and certain selection
  hm.insert(hm_f.begin(), hm_f.end());  
  
  hm_f.clear();    
  hm_f = m_HistManagerTracking.bookHistos(SystematicName, Analysis); //with some name and certain selection  
  hm.insert(hm_f.begin(), hm_f.end()); 
  
  hm_f.clear();    
  hm_f = m_HistManagerMuon.bookHistos(SystematicName, Analysis); //with some name and certain selection  
  hm.insert(hm_f.begin(), hm_f.end());   
  
  hm_f.clear();    
  hm_f = m_HistManagerElectron.bookHistos(SystematicName, Analysis); //with some name and certain selection  
  hm.insert(hm_f.begin(), hm_f.end());  
  
  hm_f.clear();    
  hm_f = m_HistManagerEvent.bookHistos(SystematicName, Analysis); //with some name and certain selection  
  hm.insert(hm_f.begin(), hm_f.end());  
    
  return hm;

}


void AnalysisManager_eeOS :: fillHistos(const EXCLCandidate* E, const xAOD::EventInfo* eventInfo, float weight, std::map<TString, TH1*> Histogram_map, TString SystematicName){

  m_HistManagerPair.fillHistos(E,       weight, Histogram_map, SystematicName, Analysis);
  
  m_HistManagerTracking.fillHistos(E,       eventInfo, weight, Histogram_map, SystematicName, Analysis); 
  
  m_HistManagerMuon.fillHistos(E,         weight, Histogram_map, SystematicName, Analysis);	
  
  m_HistManagerElectron.fillHistos(E,     weight, Histogram_map, SystematicName, Analysis); 
    
  m_HistManagerEvent.fillHistos(eventInfo, weight, Histogram_map, SystematicName, Analysis); 
  
  //m_HistManagerEXCL.fillHistos(EXCL,           weight, Histogram_map, SystematicName, Analysis); 
  
}
