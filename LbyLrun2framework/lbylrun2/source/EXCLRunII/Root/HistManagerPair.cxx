#include "EXCLRunII/HistManagerPair.h"

std::map<TString, TH1*> HistManagerPair :: bookHistos(TString CurrentShift, TString Analysis) {

//Book all histograms here

  std::map<TString, TH1*> hm;
  hm.clear();
  
  TH1* h = NULL; 
  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZMass",      "ZMass",       250,  0.,  50.); // mass [GeV]
  hm[h->GetName()] = h;  
  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZMassZoom", "ZMass Zoom",  30,  0., 30.); // mass [GeV]
  hm[h->GetName()] = h;  

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZMassLarge", "ZMass Large",  200,  0., 200.); // mass [GeV]
  hm[h->GetName()] = h; 
     
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_Zpt",	    "Zpt",         50,  0.,  25.); // pt [GeV]
  hm[h->GetName()] = h;

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptZoom",    "Zpt Zoom", 50,  0.,  5.); // pt [GeV]
  hm[h->GetName()] = h;

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptTight",    "Zpt Tight", 100,  0.,  10.); // pt [GeV]
  hm[h->GetName()] = h;
  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_Zrapidity",  "Zrapidity",    60, -3.,    3.); // rapidity
  hm[h->GetName()] = h;  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_Zabsrapidity",  "Zabsrapidity",    30, 0.,    3.); // rapidity
  hm[h->GetName()] = h;


  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZAco",	    "ZAco",         200,  0.,  1.); // acoplanarity 
  hm[h->GetName()] = h;

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZAcoZoom",   "ZAco Zoom",         100,  0.,  0.1); // acoplanarity
  hm[h->GetName()] = h;

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZDeltaEta",  "ZDeltaEta",    50, 0.,    5.); // delta eta
  hm[h->GetName()] = h; 
  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZDeltaZ0",	"Deltaz0",  100.,  -20., 20.); 
  hm[h->GetName()] = h;


  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptCluster",	    "ZptCluster",         50,  0.,  25.); 
  hm[h->GetName()] = h;

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZAcoCluster",	    "ZAcoCluster",         200,  0.,  1.); // acoplanarity 
  hm[h->GetName()] = h;

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ptSum",	    "ptSum",         100,  0.,  50.); 
  hm[h->GetName()] = h;
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ptSumCluster","ptSumCluster",         100,  0.,  50.); 
  hm[h->GetName()] = h;
  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_phAuthor","phAuthor",         3,  -0.5,  2.5);
  hm[h->GetName()] = h; 
  
/*  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptZoom_5M6",    "Zpt Zoom 1", 8,  0.,  4.); // pt [GeV]
  hm[h->GetName()] = h;  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptZoom_6M7",    "Zpt Zoom2" , 8,  0.,  4.); // pt [GeV]
  hm[h->GetName()] = h;
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptZoom_7M8",    "Zpt Zoom3", 8,  0.,  4.); // pt [GeV]
  hm[h->GetName()] = h; 
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptZoom_8M10",    "Zpt Zoom4", 8,  0.,  4.); // pt [GeV]
  hm[h->GetName()] = h;
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptZoom_10M12",    "Zpt Zoom5", 8,  0.,  4.); // pt [GeV]
  hm[h->GetName()] = h;  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptZoom_12M15",    "Zpt Zoom6", 8,  0.,  4.); // pt [GeV]
  hm[h->GetName()] = h;  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptZoom_15M30",    "Zpt Zoom7", 8,  0.,  4.); // pt [GeV]
  hm[h->GetName()] = h;
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_ZptZoom_30M",    "Zpt Zoom8", 8,  0.,  4.); // pt [GeV]
  hm[h->GetName()] = h;

  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_AcoZoom_5M6",    "Aco Zoom 1", 12,  0.,  0.06); // pt [GeV]
  hm[h->GetName()] = h;  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_AcoZoom_6M7",    "Aco Zoom2" , 12,  0.,  0.06); // pt [GeV]
  hm[h->GetName()] = h;
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_AcoZoom_7M8",    "Aco Zoom3", 12,  0.,  0.06); // pt [GeV]
  hm[h->GetName()] = h; 
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_AcoZoom_8M10",    "Aco Zoom4", 12,  0.,  0.06); // pt [GeV]
  hm[h->GetName()] = h;
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_AcoZoom_10M12",    "Aco Zoom5", 12,  0.,  0.06); // pt [GeV]
  hm[h->GetName()] = h;  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_AcoZoom_12M15",    "Aco Zoom6", 12,  0.,  0.06); // pt [GeV]
  hm[h->GetName()] = h;  
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_AcoZoom_15M30",    "Aco Zoom7", 12,  0.,  0.06); // pt [GeV]
  hm[h->GetName()] = h;
  h  = new TH1F(CurrentShift + Analysis + HM_name + "h_AcoZoom_30M",    "Aco Zoom8", 12,  0.,  0.06); // pt [GeV]
  hm[h->GetName()] = h;
 */
    
  return hm;
  
}

void HistManagerPair :: fillHistos(const EXCLCandidate* E, float weight, std::map<TString, TH1*> hm, TString CurrentShift, TString Analysis){

  //if (!Z) return;
  
  const PairCandidate *Z = E->Paircandidate();

  hm[CurrentShift + Analysis + HM_name + "h_ZMass"]	 -> Fill(Z->fourMomentum().M()/1.e3,   weight); 
  hm[CurrentShift + Analysis + HM_name + "h_ZMassZoom"]  -> Fill(Z->fourMomentum().M()/1.e3,   weight); 
  hm[CurrentShift + Analysis + HM_name + "h_ZMassLarge"] -> Fill(Z->fourMomentum().M()/1.e3,   weight);

  hm[CurrentShift + Analysis + HM_name + "h_Zpt"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight); 
  hm[CurrentShift + Analysis + HM_name + "h_ZptZoom"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight); 
  hm[CurrentShift + Analysis + HM_name + "h_ZptTight"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight); 
  hm[CurrentShift + Analysis + HM_name + "h_Zrapidity"]  -> Fill(Z->fourMomentum().Rapidity(), weight); 
  hm[CurrentShift + Analysis + HM_name + "h_Zabsrapidity"]  -> Fill(fabs(Z->fourMomentum().Rapidity()), weight); 

/*  
  float ms = Z->fourMomentum().M()/1.e3;
  if      (ms>5. && ms < 6.)   hm[CurrentShift + Analysis + HM_name + "h_ZptZoom_5M6"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight); 
  else if (ms>6. && ms < 7.)   hm[CurrentShift + Analysis + HM_name + "h_ZptZoom_6M7"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight);
  else if (ms>7. && ms < 8.)   hm[CurrentShift + Analysis + HM_name + "h_ZptZoom_7M8"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight);
  else if (ms>8. && ms < 10.)  hm[CurrentShift + Analysis + HM_name + "h_ZptZoom_8M10"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight);
  else if (ms>10. && ms < 12.) hm[CurrentShift + Analysis + HM_name + "h_ZptZoom_10M12"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight);
  else if (ms>12. && ms < 15.) hm[CurrentShift + Analysis + HM_name + "h_ZptZoom_12M15"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight);
  else if (ms>15. && ms < 30.) hm[CurrentShift + Analysis + HM_name + "h_ZptZoom_15M30"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight);  
  if (ms > 30.) 	       hm[CurrentShift + Analysis + HM_name + "h_ZptZoom_30M"]	 -> Fill(Z->fourMomentum().Pt()/1.e3,  weight);
*/

  if (Z->type() == 22) {
  
    hm[CurrentShift + Analysis + HM_name + "h_ZAco"]	 -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi(),  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ZAcoZoom"] -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi(),  weight);
    hm[CurrentShift + Analysis + HM_name + "h_ZDeltaEta"]-> Fill(fabs(Z->photon1()->eta()-Z->photon2()->eta()),  weight); 
/*    
  if      (ms>5. && ms < 6.)   hm[CurrentShift + Analysis + HM_name + "h_AcoZoom_5M6"]	 -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi(),  weight); 
  else if (ms>6. && ms < 7.)   hm[CurrentShift + Analysis + HM_name + "h_AcoZoom_6M7"]	 -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi(),  weight); 
  else if (ms>7. && ms < 8.)   hm[CurrentShift + Analysis + HM_name + "h_AcoZoom_7M8"]	 -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi(),  weight); 
  else if (ms>8. && ms < 10.)  hm[CurrentShift + Analysis + HM_name + "h_AcoZoom_8M10"]	 -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi(),  weight); 
  else if (ms>10. && ms < 12.) hm[CurrentShift + Analysis + HM_name + "h_AcoZoom_10M12"] -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi(),  weight); 
  else if (ms>12. && ms < 15.) hm[CurrentShift + Analysis + HM_name + "h_AcoZoom_12M15"] -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi(),  weight); 
  else if (ms>15. && ms < 30.) hm[CurrentShift + Analysis + HM_name + "h_AcoZoom_15M30"] -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi(),  weight);  
  if (ms > 30.) 	       hm[CurrentShift + Analysis + HM_name + "h_AcoZoom_30M"]	 -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->photon2()->phi()))/TMath::Pi(),  weight); 
*/    
  
    hm[CurrentShift + Analysis + HM_name + "h_ZAcoCluster"]-> Fill(1. - acos(cos(Z->photon1()->caloCluster()->phi()-Z->photon2()->caloCluster()->phi()))/TMath::Pi(),  weight);
    
      TLorentzVector el1, el2;
      el1.SetPtEtaPhiM(Z->photon1()->caloCluster()->e()/cosh(Z->photon1()->caloCluster()->eta()),  Z->photon1()->caloCluster()->eta(),
		       Z->photon1()->caloCluster()->phi(), Z->photon1()->m()); 
      el2.SetPtEtaPhiM(Z->photon2()->caloCluster()->e()/cosh(Z->photon2()->caloCluster()->eta()),  Z->photon2()->caloCluster()->eta(),
		       Z->photon2()->caloCluster()->phi(), Z->photon2()->m()); 
      TLorentzVector Zee = el1+el2;
      
    hm[CurrentShift + Analysis + HM_name + "h_ZptCluster"]-> Fill(Zee.Pt()/1.e3,  weight); 
    
    hm[CurrentShift + Analysis + HM_name + "h_ptSum"]-> Fill(Z->photon1()->pt()/1.e3+Z->photon2()->pt()/1.e3,  weight);
    hm[CurrentShift + Analysis + HM_name + "h_ptSumCluster"]-> Fill(Z->photon1()->caloCluster()->pt()/1.e3+Z->photon2()->caloCluster()->pt()/1.e3,  weight);
  
    if(Z->photon1()->author() & xAOD::EgammaParameters::AuthorPhoton) hm[CurrentShift + Analysis + HM_name + "h_phAuthor"]-> Fill(0);
    if(Z->photon1()->author() & xAOD::EgammaParameters::AuthorAmbiguous) hm[CurrentShift + Analysis + HM_name + "h_phAuthor"]-> Fill(1);
    if(Z->photon1()->author() & xAOD::EgammaParameters::AuthorCaloTopo35) hm[CurrentShift + Analysis + HM_name + "h_phAuthor"]-> Fill(2);

    if(Z->photon2()->author() & xAOD::EgammaParameters::AuthorPhoton) hm[CurrentShift + Analysis + HM_name + "h_phAuthor"]-> Fill(0);
    if(Z->photon2()->author() & xAOD::EgammaParameters::AuthorAmbiguous) hm[CurrentShift + Analysis + HM_name + "h_phAuthor"]-> Fill(1);
    if(Z->photon2()->author() & xAOD::EgammaParameters::AuthorCaloTopo35) hm[CurrentShift + Analysis + HM_name + "h_phAuthor"]-> Fill(2);
  
  } 


  if (Z->type() == 1122) {
  
    hm[CurrentShift + Analysis + HM_name + "h_ZAco"]	 -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->electron1()->phi()))/TMath::Pi(),  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ZAcoZoom"] -> Fill(1. - acos(cos(Z->photon1()->phi()-Z->electron1()->phi()))/TMath::Pi(),  weight);
    hm[CurrentShift + Analysis + HM_name + "h_ZDeltaEta"]-> Fill(fabs(Z->photon1()->eta()-Z->electron1()->eta()),  weight); 
  
    hm[CurrentShift + Analysis + HM_name + "h_ZAcoCluster"]-> Fill(1. - acos(cos(Z->photon1()->caloCluster()->phi()-Z->electron1()->caloCluster()->phi()))/TMath::Pi(),  weight);
    
      TLorentzVector el1, el2;
      el1.SetPtEtaPhiM(Z->photon1()->caloCluster()->e()/cosh(Z->photon1()->caloCluster()->eta()),  Z->photon1()->caloCluster()->eta(),
		       Z->photon1()->caloCluster()->phi(), Z->photon1()->m()); 
      el2.SetPtEtaPhiM(Z->electron1()->caloCluster()->e()/cosh(Z->electron1()->caloCluster()->eta()),  Z->electron1()->caloCluster()->eta(),
		       Z->electron1()->caloCluster()->phi(), Z->electron1()->m()); 
      TLorentzVector Zee = el1+el2;
      
    hm[CurrentShift + Analysis + HM_name + "h_ZptCluster"]-> Fill(Zee.Pt()/1.e3,  weight); 
    
    hm[CurrentShift + Analysis + HM_name + "h_ptSum"]-> Fill(Z->photon1()->pt()/1.e3+Z->electron1()->pt()/1.e3,  weight);
    hm[CurrentShift + Analysis + HM_name + "h_ptSumCluster"]-> Fill(Z->photon1()->caloCluster()->pt()/1.e3+Z->electron1()->caloCluster()->pt()/1.e3,  weight);
    
    if(Z->photon1()->author() & xAOD::EgammaParameters::AuthorPhoton) hm[CurrentShift + Analysis + HM_name + "h_phAuthor"]-> Fill(0);
    if(Z->photon1()->author() & xAOD::EgammaParameters::AuthorAmbiguous) hm[CurrentShift + Analysis + HM_name + "h_phAuthor"]-> Fill(1);
    if(Z->photon1()->author() & xAOD::EgammaParameters::AuthorCaloTopo35) hm[CurrentShift + Analysis + HM_name + "h_phAuthor"]-> Fill(2); 
  } 
  
  
  if (Z->type() == 11 || Z->type() == 1111) {
  
    hm[CurrentShift + Analysis + HM_name + "h_ZAco"]	 -> Fill(1. - acos(cos(Z->electron1()->phi()-Z->electron2()->phi()))/TMath::Pi(),  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ZAcoZoom"] -> Fill(1. - acos(cos(Z->electron1()->phi()-Z->electron2()->phi()))/TMath::Pi(),  weight);
    hm[CurrentShift + Analysis + HM_name + "h_ZDeltaEta"]-> Fill(fabs(Z->electron1()->eta()-Z->electron2()->eta()),  weight); 
    
    const xAOD::TrackParticle* tp1 = Z->electron1()->trackParticle();
    const xAOD::TrackParticle* tp2 = Z->electron2()->trackParticle(); 
    hm[CurrentShift + Analysis + HM_name + "h_ZDeltaZ0"]-> Fill( (tp1->z0() + tp1->vz()) - (tp2->z0() + tp2->vz()),  weight);
    
    hm[CurrentShift + Analysis + HM_name + "h_ZAcoCluster"]-> Fill(1. - acos(cos(Z->electron1()->caloCluster()->phi()-Z->electron2()->caloCluster()->phi()))/TMath::Pi(),  weight);
    
      TLorentzVector el1, el2;
      el1.SetPtEtaPhiM(Z->electron1()->caloCluster()->e()/cosh(Z->electron1()->caloCluster()->eta()),  Z->electron1()->caloCluster()->eta(),
		       Z->electron1()->caloCluster()->phi(), Z->electron1()->m()); 
      el2.SetPtEtaPhiM(Z->electron2()->caloCluster()->e()/cosh(Z->electron2()->caloCluster()->eta()),  Z->electron2()->caloCluster()->eta(),
		       Z->electron2()->caloCluster()->phi(), Z->electron2()->m()); 
      TLorentzVector Zee = el1+el2;
      
    hm[CurrentShift + Analysis + HM_name + "h_ZptCluster"]-> Fill(Zee.Pt()/1.e3,  weight); 

    hm[CurrentShift + Analysis + HM_name + "h_ptSum"]-> Fill(Z->electron1()->pt()/1.e3+Z->electron1()->pt()/1.e3,  weight);
    hm[CurrentShift + Analysis + HM_name + "h_ptSumCluster"]-> Fill(Z->electron1()->caloCluster()->pt()/1.e3+Z->electron1()->caloCluster()->pt()/1.e3,  weight);
  
  }  

  if (Z->type() == 13 || Z->type() == 1313) {
  
    hm[CurrentShift + Analysis + HM_name + "h_ZAco"]	 -> Fill(1. - acos(cos(Z->muon1()->phi()-Z->muon2()->phi()))/TMath::Pi(),  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ZAcoZoom"] -> Fill(1. - acos(cos(Z->muon1()->phi()-Z->muon2()->phi()))/TMath::Pi(),  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ZDeltaEta"]-> Fill(fabs(Z->muon1()->eta()-Z->muon2()->eta()),  weight); 
    
    const xAOD::TrackParticle *mutrk1 = Z->muon1()->primaryTrackParticle();
    const xAOD::TrackParticle *mutrk2 = Z->muon2()->primaryTrackParticle();
    hm[CurrentShift + Analysis + HM_name + "h_ZDeltaZ0"]-> Fill( (mutrk1->z0() + mutrk1->vz()) - (mutrk2->z0() + mutrk2->vz()),  weight); 	
	
  } 

  if (Z->type() == -1113 || Z->type() == 1311) {
  
    hm[CurrentShift + Analysis + HM_name + "h_ZAco"]	 -> Fill(1. - acos(cos(Z->muon1()->phi()-Z->electron1()->phi()))/TMath::Pi(),  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ZAcoZoom"] -> Fill(1. - acos(cos(Z->muon1()->phi()-Z->electron1()->phi()))/TMath::Pi(),  weight); 
    hm[CurrentShift + Analysis + HM_name + "h_ZDeltaEta"]-> Fill(fabs(Z->muon1()->eta()-Z->electron1()->eta()),  weight); 
  } 

}

