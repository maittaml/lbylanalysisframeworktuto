#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

#inputFilePath = '/sps/atlas/m/mdyndal/data_UPC_HI_rel21/15/cep2gg/420255/'
#ROOT.SH.ScanDir().filePattern( '*root*' ).scan( sh, inputFilePath )

inputFilePath = '/sps/atlas/m/mdyndal/data_UPC_HI_rel21/15/starlight_ee/mc16_valid.420052.Starlight_r193_gammagamma2ee_breakupMode2.recon.AOD.e4995_s2820_r10933_r10990/'
ROOT.SH.ScanDir().filePattern( '*AOD.15861171._000001.pool.root.1*' ).scan( sh, inputFilePath )

#inputFilePath = '/sps/atlas/m/mdyndal/data_UPC_HI_rel21/15/data18/user.igrabows.data18_hi.00367273.physics_UPC.merge.AOD.f1028_m2048.HION4New.v01_EXT0_56mub/'
#ROOT.SH.ScanDir().filePattern( '*.root*' ).scan( sh, inputFilePath )
sh.Print()

# Create an EventLoop job.
job = ROOT.EL.Job()
#job.useXAOD()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, -1 )


# Create the algorithm's configuration. Note that we'll be able to add
# algorithm property settings here later on.
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
config = AnaAlgorithmConfig( 'LapxAODEvent/AnalysisAlg' )
config2 = AnaAlgorithmConfig( 'EXCLRecoAnalysis/AnalysisAlg2' )

#job.algsAdd( config )
job.algsAdd( config2 )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
