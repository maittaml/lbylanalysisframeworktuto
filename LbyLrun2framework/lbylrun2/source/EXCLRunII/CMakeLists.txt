# The name of the package:
atlas_subdir (EXCLRunII)

atlas_install_xmls(data/GRL/*.xml)
atlas_install_data(data/*.model)

# Add the shared library:
atlas_add_library (EXCLRunIILib
  EXCLRunII/*.h Root/*.cxx
  PUBLIC_HEADERS EXCLRunII
  LINK_LIBRARIES ElectronPhotonShowerShapeFudgeToolLib ElectronPhotonFourMomentumCorrectionLib TrigDecisionToolLib TrigConfxAODLib TrigConfInterfaces AnaAlgorithmLib AsgAnalysisInterfaces EventLoop xAODRootAccess xAODEventInfo xAODTrigMinBias xAODJet xAODForward xAODHIEvent xAODTrigL1Calo xAODMuon  xAODEgamma xAODCutFlow  )


# Add the dictionary:
atlas_add_dictionary (EXCLRunIIDict
  EXCLRunII/EXCLRunIIDict.h
  EXCLRunII/selection.xml
  LINK_LIBRARIES EXCLRunIILib)
