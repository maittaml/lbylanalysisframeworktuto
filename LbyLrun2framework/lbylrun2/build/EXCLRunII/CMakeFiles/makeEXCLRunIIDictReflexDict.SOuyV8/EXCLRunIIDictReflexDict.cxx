// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME EXCLRunIIDictReflexDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/eos/user/m/maittaml/LbyLframeworkrun2/lbyl_2nd_paper/source/EXCLRunII/EXCLRunII/EXCLRunIIDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *LapxAODEvent_Dictionary();
   static void LapxAODEvent_TClassManip(TClass*);
   static void delete_LapxAODEvent(void *p);
   static void deleteArray_LapxAODEvent(void *p);
   static void destruct_LapxAODEvent(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::LapxAODEvent*)
   {
      ::LapxAODEvent *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::LapxAODEvent));
      static ::ROOT::TGenericClassInfo 
         instance("LapxAODEvent", "LapxAODEvent.h", 134,
                  typeid(::LapxAODEvent), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &LapxAODEvent_Dictionary, isa_proxy, 4,
                  sizeof(::LapxAODEvent) );
      instance.SetDelete(&delete_LapxAODEvent);
      instance.SetDeleteArray(&deleteArray_LapxAODEvent);
      instance.SetDestructor(&destruct_LapxAODEvent);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::LapxAODEvent*)
   {
      return GenerateInitInstanceLocal((::LapxAODEvent*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::LapxAODEvent*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *LapxAODEvent_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::LapxAODEvent*)0x0)->GetClass();
      LapxAODEvent_TClassManip(theClass);
   return theClass;
   }

   static void LapxAODEvent_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *EXCLRecoAnalysis_Dictionary();
   static void EXCLRecoAnalysis_TClassManip(TClass*);
   static void delete_EXCLRecoAnalysis(void *p);
   static void deleteArray_EXCLRecoAnalysis(void *p);
   static void destruct_EXCLRecoAnalysis(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::EXCLRecoAnalysis*)
   {
      ::EXCLRecoAnalysis *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::EXCLRecoAnalysis));
      static ::ROOT::TGenericClassInfo 
         instance("EXCLRecoAnalysis", "EXCLRecoAnalysis.h", 101,
                  typeid(::EXCLRecoAnalysis), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &EXCLRecoAnalysis_Dictionary, isa_proxy, 4,
                  sizeof(::EXCLRecoAnalysis) );
      instance.SetDelete(&delete_EXCLRecoAnalysis);
      instance.SetDeleteArray(&deleteArray_EXCLRecoAnalysis);
      instance.SetDestructor(&destruct_EXCLRecoAnalysis);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::EXCLRecoAnalysis*)
   {
      return GenerateInitInstanceLocal((::EXCLRecoAnalysis*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::EXCLRecoAnalysis*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *EXCLRecoAnalysis_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::EXCLRecoAnalysis*)0x0)->GetClass();
      EXCLRecoAnalysis_TClassManip(theClass);
   return theClass;
   }

   static void EXCLRecoAnalysis_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_LapxAODEvent(void *p) {
      delete ((::LapxAODEvent*)p);
   }
   static void deleteArray_LapxAODEvent(void *p) {
      delete [] ((::LapxAODEvent*)p);
   }
   static void destruct_LapxAODEvent(void *p) {
      typedef ::LapxAODEvent current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::LapxAODEvent

namespace ROOT {
   // Wrapper around operator delete
   static void delete_EXCLRecoAnalysis(void *p) {
      delete ((::EXCLRecoAnalysis*)p);
   }
   static void deleteArray_EXCLRecoAnalysis(void *p) {
      delete [] ((::EXCLRecoAnalysis*)p);
   }
   static void destruct_EXCLRecoAnalysis(void *p) {
      typedef ::EXCLRecoAnalysis current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::EXCLRecoAnalysis

namespace {
  void TriggerDictionaryInitialization_libEXCLRunIIDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libEXCLRunIIDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$EXCLRunII/LapxAODEvent.h")))  __attribute__((annotate("$clingAutoload$EXCLRunII/EXCLRecoAnalysis.h")))  LapxAODEvent;
class __attribute__((annotate("$clingAutoload$EXCLRunII/EXCLRecoAnalysis.h")))  EXCLRecoAnalysis;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libEXCLRunIIDict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "EXCLRunII-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ EXCLRunII-00-00-00
#endif
#ifndef EIGEN_DONT_VECTORIZE
  #define EIGEN_DONT_VECTORIZE 1
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef EXCLRUNII_EXCLRUNII_DICT_H
#define EXCLRUNII_EXCLRUNII_DICT_H

// This file includes all the header files that you need to create
// dictionaries for.


#include <EXCLRunII/EXCLRecoAnalysis.h>
#include <EXCLRunII/LapxAODEvent.h>




#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"EXCLRecoAnalysis", payloadCode, "@",
"LapxAODEvent", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libEXCLRunIIDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libEXCLRunIIDict_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libEXCLRunIIDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libEXCLRunIIDict() {
  TriggerDictionaryInitialization_libEXCLRunIIDict_Impl();
}
