#!/usr/bin/env python

# Initial imports
import argparse
import ROOT
import re
import os
import shutil
import sys

# Take some arguments from the command line:
parser = argparse.ArgumentParser( description = "VBF invisible ntuple making job", add_help=True , fromfile_prefix_chars='@')
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
config = AnaAlgorithmConfig( 'LapxAODEvent/AnalysisAlg' )
config2 = AnaAlgorithmConfig( 'EXCLRecoAnalysis/AnalysisAlg2' )

#job.algsAdd( config )
#job.algsAdd( config2 )

# Submit job
parser.add_argument( "-s", "--submitDir", type = str, dest = "submitDir", default = "submitDir", help = "Submission directory" )
parser.add_argument( "-v", "--version", type = str, dest = "version", default = "vXX", help = "Ntuple version in the grid" )
parser.add_argument( "-f", "--inputFile", type = str, dest = "input", default = "", help = "Input file to process" )
parser.add_argument( "-t", "--inputList", type = str, dest = "txt", default = "", help = "Comma-separated list of text files containing file paths" )
parser.add_argument( "-i", "--inputDirs", type = str, dest = "dir", default = "", help = "Comma-separated list of input directories (they will be scanned for samples, one per subdirectory)" )
parser.add_argument( "-g", "--inputRucio", type = str, dest="rucio", default = "", help = "Comma-separated list of input Rucio datasets" )
parser.add_argument( "-l", "--inputRucioLists", type = str, dest="ruciolist", default = "", help = "Comma-separated list of text files containing one Rucio sample per line (empty lines or lines starting with # are ignored)" )
parser.add_argument( "-n", "--nevents", type = int, dest = 'nmax', help="Maximum number of events to process for all the datasets")
parser.add_argument( "-nf", "--nfiles", type = int, dest = 'nfiles', help="number of files per job")
parser.add_argument( "-k", "--nskip", type = int, dest = "nskip", help="Number of events to skip for all the datasets")
parser.add_argument( "-r", "--replicationSite", type = str, dest = "replicationSite", default = "DESY-HH_LOCALGROUPDISK", help="Name of disk where to replicate output of grid jobs" )
parser.add_argument( "-w", "--overwrite", dest = "overwrite", action = "store_false", default = True, help = "don't overwrite previous submitDir")
parser.add_argument( "-d", "--driver", type = str, dest = "driver", default = "local", choices = ["local", "prun","condor"], help = "driver to be used (local, prun, condor)", metavar="driver")
#parser.add_argument( "-a", "--algo", type = str, dest = "algoName", default = "EXCLRecoAnalysis/AnalysisAlg2", choices = ["VBFInv","VBFInvTruth", "VBFInvSherpaTruth", "VBFInvVjetsRW"], help = "algorithm name (e.g. VBFInv, VBFInvTruth, VBFInvSherpaTruth, VBFInvVjetsRW)")
parser.add_argument( "-u", "--user", type=str, dest="userName", default=os.environ["USER"], help="username for grid jobs", metavar="userName")



args, unknown = parser.parse_known_args()

print "\nArguments used:"
print "------------------------------------------------"
print "Command entered: " , " ".join(sys.argv[:])
print "Configuration:"
print args
print "------------------------------------------------"
print "\n"

# Overwrite submitDir
if args.overwrite:
    shutil.rmtree(args.submitDir, True)

#Set up the job for xAOD access:
ROOT.xAOD.Init().ignore()

# Set up the sample handler object:
sh = ROOT.SH.SampleHandler()

if args.input:
    chain = ROOT.TChain( "CollectionTree" )
    chain.Add( args.input )
    sampleName = args.input.split('/')[-2].split('.')
    if len(sampleName)==1:
        sampleName = '.'.join(args.input.split('/')[-2].split('.'))
    else:
        sampleName = '.'.join(args.input.split('/')[-2].split('.')[:-3])
    print 'sampleName: ',sampleName
    print args.input.split('/')[-2]
    print args.input.split('/')[-2].split('.')
    sh.add( ROOT.SH.makeFromTChain( sampleName, chain ) )
getlist = lambda x: filter(lambda y: y != '', x.replace(' ', '').split(','))
txts = getlist(args.txt)
for txt in txts:
  sampleName = ''.join(txt.split('/')[-1].split('.')[:-1]) # /a/b/c/d.txt -> d
  print 'adding sample path from text file ',sampleName
  ROOT.SH.readFileList(sh, sampleName, txt)
rucios = getlist(args.rucio)
for rucio in rucios:
  print 'adding rucio dataset ',rucio
  ROOT.SH.scanRucio(sh, rucio)
ruciolists = getlist(args.ruciolist)
print ruciolists
for ruciolist in ruciolists:
  print 'adding rucio datasets from text file ',ruciolist
  for rucioraw in open(ruciolist).readlines():
    rucio = rucioraw.rstrip('\n').replace(' ', '') # remove spaces from line
    if rucio.startswith('#') == False and rucio != '': # ignore comments / separators
      if (args.driver == "condor"):
        ROOT.SH.addGrid(sh,rucio)
      else:
        ROOT.SH.scanRucio(sh, rucio)
if args.driver == "condor" and ruciolists != '':
  ROOT.SH.makeGridDirect(sh,args.replicationSite,"root://dcache-atlas-xrootd.desy.de:1094/","", False)
dirs = getlist(args.dir)
for dir in dirs:
  print 'adding directory with sample ',dir
  ROOT.SH.scanDir(sh, dir)
#check if it is AFII or Data:
regexps = { 'isAFII' : re.compile('a\d\d\d'),
             'isData': re.compile('data'),
 }
for isample in xrange(sh.size()):
  sample = sh.at(isample)
  print sample.name()
  for opt,this_re in regexps.items():
    state = False
    if this_re.search(sample.name()):
      state = True
      print 'CheckingMC - found: ',opt,' sample: ',isample
    sample.setMetaDouble(opt, state)
    print 'Sample {name} has {opt} set to {val}'.format(name=sample.name(), opt=opt, val=state)
sh.setMetaString( "nc_tree", "CollectionTree" )
# print SampleHandler object
getattr(sh, 'print')()

# Create an EventLoop job:
job = ROOT.EL.Job()
job.sampleHandler( sh )

# Add the analysis algorithm:
config = AnaAlgorithmConfig( 'LapxAODEvent/AnalysisAlg' )
config2 = AnaAlgorithmConfig( 'EXCLRecoAnalysis/AnalysisAlg2' )

#job.algsAdd( config )
job.algsAdd( config2 )




# define an output
output = ROOT.EL.OutputStream  ("MiniNtuple")
outputHist = ROOT.EL.OutputStream ("hist")
job.outputAdd (output);
job.outputAdd (outputHist)

# Run the job:
if (args.driver == 'local'):
    driver = ROOT.EL.DirectDriver()
    driver.submit( job, args.submitDir )
elif (args.driver == 'prun'):
    tag=args.version
   # if args.doSystematics:
    #  tag = tag+"Syst"
    dset_name_mask = 'user.{user}.{tag}.%in:name[2]%.%in:name[3]%.%in:name[6]%'.format(user=args.userName, tag=tag)
    print dset_name_mask, len(dset_name_mask)
    driver = ROOT.EL.PrunDriver()
    #if args.doSystematics:
    driver.options().setString(ROOT.EL.Job.optGridNGBPerJob, '40')
    driver.options().setString('nc_optGridNfilesPerJob', '40')
      #--nGBPerJob=4
    #else:
     # driver.options().setString(ROOT.EL.Job.optGridNGBPerJob, '5')
      #driver.options().setString('nc_optGridNfilesPerJob', '5')
      #--nGBPerJob=10
    driver.options().setString('nc_outputSampleName', dset_name_mask)
    #driver.options().setString("nc_optGridDestSE","DESY-HH_LOCALGROUPDISK")
    #driver.options().setString("nc_optGridDestSE","MWT2_UC_LOCALGROUPDISK")
    if args.replicationSite != None:
        driver.options().setString('nc_destSE', args.replicationSite)
    driver.submitOnly(job, args.submitDir )
elif (args.driver == 'condor'):
    driver = ROOT.EL.CondorDriver()
    condor_options="+MyProject = \"af-atlas\"" + "\n"
    condor_options+="+RequestRuntime = 50000" + "\n"
    condor_options+="RequestMemory = 2G" + "\n"
    condor_options+="RequestDisk = 500M" + "\n"
    condor_options+="notify_user = hassane.hamdaoui@cern.ch" + "\n"
    condor_options+="notification = Error" + "\n"
    condor_options+="should_transfer_files = NO" + "\n"
    condor_options+="Requirements = ( OpSysAndVer == \"SL6\")" + "\n"
    #  Not working well OpSysAndVer == \"CentOS7\" ||
    #    condor_options+="" + "\n"
    driver.options().setString (ROOT.EL.Job.optCondorConf, condor_options);
    driver.shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";
    driver.submitOnly (job, args.submitDir);
else:
    raise RuntimeError('Unrecognized driver option {opt}'.format(opt=args.driver))

